#!/usr/bin/env python
#
# The processing/waiting page of the app.
#


import os
import urllib
import webapp2
import jinja2
import globals
import datastore
import functions
import pickle
import time
from math import floor
import logging


from google.appengine.ext import ndb
from google.appengine.api import taskqueue
from google.appengine.api import memcache
from google.appengine.ext import blobstore
from google.appengine.api import users
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.api import images as GAEimages

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


class MainHandler(webapp2.RequestHandler):

    def get(self):
        job_id = int(self.request.get('job_id'))

        if globals.VERBOSE:
            logging.info("TESTING #######            processing MainHandler start")
            logging.info("--- job_id: %d" % (job_id))

        job = functions.get_job(job_id)
        logging.info('processing has started for job %d' % job_id)

        primes = [x for x in functions.primes_sieve(globals.MAX_PRIME_TASKS)]

        # Get image properties
        img_data = blobstore.fetch_data(job.source_blob_key, 0, 50000)
        img = GAEimages.Image(image_data=img_data)
        width, height = [img.width, img.height]
        if globals.VERBOSE: logging.info("Image dimensions (%d, %d)" % (width, height))
        # Calculate size and number of patches
        num_horizontal_patches = max(globals.MIN_NUMBER_OF_PATCHES, width/globals.MAX_PATCH_SIZE)
        num_vertical_patches = max(globals.MIN_NUMBER_OF_PATCHES, height/globals.MAX_PATCH_SIZE)
        if num_horizontal_patches > globals.MAX_NUMBER_OF_PATCHES or num_vertical_patches > globals.MAX_NUMBER_OF_PATCHES:
            # Rescale to fit within 80x80 patches
            ratio = float(num_horizontal_patches) / float(num_vertical_patches)
            if num_horizontal_patches > num_vertical_patches:
                num_horizontal_patches = globals.MAX_NUMBER_OF_PATCHES
                num_vertical_patches = num_horizontal_patches / ratio
            else:
                num_vertical_patches = globals.MAX_NUMBER_OF_PATCHES
                num_horizontal_patches = num_vertical_patches * ratio
            num_horizontal_patches = int(round(num_horizontal_patches))
            num_vertical_patches = int(round(num_vertical_patches))
            logging.info("Rescaled nr patches: ratio  %f\tw %f\th %f" % (ratio, num_horizontal_patches, num_vertical_patches))

        num_patches_total = num_horizontal_patches*num_vertical_patches

        job.nr_patches_wide = num_horizontal_patches
        job.nr_patches_high = num_vertical_patches
        job.patch_height = int(floor(height/num_vertical_patches))
        job.patch_width = int(floor(width/num_horizontal_patches))

        res_factor = globals.TILE_RESOLUTION_FACTOR
        total_width = job.patch_width*job.nr_patches_wide*res_factor
        total_height = job.patch_height*job.nr_patches_high*res_factor
        if max(total_height,total_width) > globals.MAX_DIMENSION:
            # Rescale to fit within globals.MAX_DIMENSION x globals.MAX_DIMENSION
            res_factor_wide = globals.MAX_DIMENSION / (job.patch_width*job.nr_patches_wide)
            res_factor_high = globals.MAX_DIMENSION / (job.patch_height*job.nr_patches_high)
            res_factor = min(res_factor_high,res_factor_wide) * 0.99
            total_width = job.patch_width*job.nr_patches_wide*res_factor
            total_height = job.patch_height*job.nr_patches_high*res_factor

        job.tile_resolution_factor = res_factor
        job.put()

        ## PROCESS SOURCE IMAGE
        if not functions.is_done(job_id,'process_source',globals.NR_TASKS_SOURCE,primes):
            key = '%d:%s_counter' % (job_id, 'process_source')
            counter_value = memcache.get(key)
            if counter_value is None:
                if not memcache.add(key, 1, globals.MEMCACHE_TIMEOUT):
                    logging.error('processing:: source could not add memcache key: %s' % key)
                    return False

            # Update job with patch dimensions.
            patches_per_task = num_patches_total/globals.NR_TASKS_SOURCE
            for i in range(globals.NR_TASKS_SOURCE):
                y_start=i*patches_per_task
                y_end=(i+1)*patches_per_task # exclusive
                if i == (globals.NR_TASKS_SOURCE-1):
                    y_end = num_patches_total
                query_params = {'job_id': job_id,
                                'prime': primes[i],
                                'y_start': y_start,
                                'y_end': y_end}
                taskqueue.Task(url='/process_source/', params=query_params, method='GET').add(queue_name='process-source-queue')
                time.sleep(globals.WAIT_LAUNCH)


        ## PROCESS TILES
        if not functions.is_done(job_id,'process_tiles',globals.NR_TASKS_TILES,primes):
            key = '%d:%s_counter' % (job_id, 'process_tiles')
            counter_value = memcache.get(key)
            if counter_value is None:
                if not memcache.add(key, 1, globals.MEMCACHE_TIMEOUT):
                    logging.error('processing:: tiles could not add memcache key: %s' % key)
                    return False

            for i in range(globals.NR_TASKS_TILES):
                tile_start=i*10
                tile_end=(i+1)*10
                query_params = {'job_id': job_id,
                                'prime': primes[i],
                                'tile_start': tile_start,
                                'tile_end': tile_end}
                taskqueue.Task(url='/process_tiles/', params=query_params, method='GET').add(queue_name='process-tiles-queue')
                time.sleep(globals.WAIT_LAUNCH)

        ## PATCH TILE MAPPING
        patches_per_map_task = num_patches_total/globals.NR_TASKS_MAPPING
        if not functions.is_done(job_id,'mapping',globals.NR_TASKS_MAPPING,primes):
            key = '%d:%s_counter' % (job_id, 'mapping')
            counter_value = memcache.get(key)
            if counter_value is None:
                if not memcache.add(key, 1, globals.MEMCACHE_TIMEOUT):
                    logging.error('processing:: could not add memcache key: %s' % key)
                    return False

            for i in range(globals.NR_TASKS_MAPPING):
                patch_start=i*patches_per_map_task
                patch_end=(i+1)*patches_per_map_task
                if i == (globals.NR_TASKS_MAPPING-1):
                    patch_end = num_patches_total
                query_params = {'job_id': job_id,
                                'prime': primes[i],
                                'patch_start': patch_start,
                                'patch_end': patch_end}
                taskqueue.Task(url='/patch_tile_mapping/wait/', params=query_params, method='GET').add(queue_name='waiting-queue')
                time.sleep(globals.WAIT_LAUNCH)

        ## PARALLEL CONSTRUCT IMAGE
        patches_per_construct_task = num_patches_total/globals.NR_TASKS_SUBCONSTRUCT
        # num_trailing_patches = divmod(job.nr_patches_wide - divmod(patches_per_construct_task, job.nr_patches_wide),job.nr_patches_wide)
        if not functions.is_done(job_id, 'region_constructing', globals.NR_TASKS_SUBCONSTRUCT, primes):
            key = '%d:%s_counter' % (job_id, 'region_constructing')
            counter_value = memcache.get(key)
            if counter_value is None:
                if not memcache.add(key, 1, globals.MEMCACHE_TIMEOUT):
                    logging.error('processing:: tiles could not add region key: %s' % key)
                    return False

            for i in range(globals.NR_TASKS_SUBCONSTRUCT):
                patch_start=i*patches_per_construct_task
                patch_end=(i+1)*patches_per_construct_task
                if i == (globals.NR_TASKS_SUBCONSTRUCT-1):
                    patch_end = num_patches_total
                query_params = {'job_id': job_id,
                                'prime': primes[i],
                                'patch_start': patch_start,
                                'patch_end': patch_end}
                taskqueue.Task(url='/construct_image/region/wait/', params=query_params, method='GET').add(queue_name='construct-queue')


        ## CONSTRUCT IMAGE
        query_params = {'job_id': job_id}
        taskqueue.Task(url='/construct_image/wait/', params=query_params, method='GET').add(queue_name='waiting-queue')


        ## CLEAN UP RESULTS
        query_params = {'job_id': job_id}
        taskqueue.Task(url='/construct_image/clean_up/wait/', params=query_params, method='GET').add(queue_name='waiting-queue')

        query_params = {'job_id': job_id}
        self.redirect('/processing/waiting/?' + urllib.urlencode(query_params))

        if globals.VERBOSE: logging.info("TESTING #######            processing MainHandler done")


class WaitingHandler(webapp2.RequestHandler):

    def get(self):
        job_id = int(self.request.get('job_id'))

        if globals.VERBOSE: logging.info("TESTING #######            processing WaitingHandler start")

        query_params = {'job_id': job_id}
        self.redirect('/result/?' + urllib.urlencode(query_params))

        if globals.VERBOSE: logging.info("TESTING #######            processing WaitingHandler done")




app = webapp2.WSGIApplication([
    ('/processing/', MainHandler),
    ('/processing/waiting/', WaitingHandler),
], debug=True)
