__author__ = 'bemasv'

#
# Processes the tiles and calculates the metric for each processed tile.
#
# The tiles are fetched, cropped to a square aspect ratio and resized to 'globals.tile_resoltion'.
# The calculated metric is stored in the datastore under:
# search_term -> index -> metric
# The metric is also appended to 'search_term'_metrics in memcache which is a list
# consisting of pairs with 'index : metric'
#

from google.appengine.api import memcache
import globals
import datastore
import functions
import webapp2
import urllib2
import logging
import urllib
import StringIO
import json as simplejson
import pickle
from PIL import Image as PILImage
from google.appengine.api import users

prime_colors = {2 : 'red',
                3 : 'orange',
                5 : 'yellow',
                7 : 'green',
                11 : 'blue',
                13 : 'purple',
                17 : 'pink',
                19 : 'white',
                23 : 'gray',
                37 : 'black',
                29 : 'brown',
                31 : 'teal',
                }

def get_thumbnail_urls(search_terms, index,
                            search_engine_id=globals.SEARCH_ENGINE_ID,
                            api_key=globals.API_KEY):
    """
    get_thumbnail_urls(search_terms)

    Implements caching of results from custom search API calls to reduce number of calls.

    :param search_terms: Input query to the google image search API
    :return: thumbnail_url_list: List of urls from performing an image search on search_terms
    """
    # Check the memcache for search results
    if globals.COLOR_SEARCH:
        search_terms_id = ("image_search:%s:%s:" % (search_terms, prime_colors[index]))
    else:
        search_terms_id = ("image_search:%s:%d:" % (search_terms, index))
    pickled_thumbnail_url_list = memcache.get(search_terms_id)
    if pickled_thumbnail_url_list is not None:
        if globals.VERBOSE: logging.info("---- process_tiles: results retrieved from memcache")
        thumbnail_url_list = pickle.loads(pickled_thumbnail_url_list)
        return thumbnail_url_list
    else:
        # If not in the memcache, check the datastore
        search_results = datastore.SearchTermResults.get_by_id(search_terms_id)
        if search_results is not None:
            pickled_thumbnail_url_list = search_results.pickled_thumbnail_url_list
            if pickled_thumbnail_url_list is not None:
                memcache.add(search_terms_id, search_results.pickled_thumbnail_url_list)
                thumbnail_url_list = pickle.loads(pickled_thumbnail_url_list)
                if globals.VERBOSE: logging.info("----- process_tiles: results retrieved from ds.")
                return thumbnail_url_list
            else:
                if globals.VERBOSE: logging.info("----- process_tiles: pickles unavailable. What is this?")
        else:
            # If in neither memcache nor datastore, perform a search.
            thumbnail_url_list = []
            request = generate_search_request(search_terms, index,
                                              search_engine_id=search_engine_id,
                                              api_key=api_key)
            response = urllib2.urlopen(request, timeout=globals.URL_TIMEOUT)
            # Get results using JSON
            results = simplejson.load(response)
            # TODO Error code 403 = exceed daily quota
            data = results['items']
            for item in data:
                thumbnail_url_list.append(item['image']['thumbnailLink'])
            pickled_results = pickle.dumps(thumbnail_url_list)
            memcache.add(search_terms_id, pickled_results)
            image_search_results_item = datastore.SearchTermResults(id=search_terms_id, pickled_thumbnail_url_list=pickled_results)
            image_search_results_item.put()
            if globals.VERBOSE: logging.info("---- process_tiles: results retrieved via search API.")
            return thumbnail_url_list

def generate_search_request(search_term, index,
                            search_engine_id=globals.SEARCH_ENGINE_ID,
                            api_key=globals.API_KEY):
    url = 'https://www.googleapis.com/customsearch/v1?q='
    url += urllib.quote(search_term)
    url += '&cx='
    url += search_engine_id
    url += '&fileType=jpg&searchType=image'
    url += '&key=' + api_key
    if globals.COLOR_SEARCH:
        url += '&tbs=ic:specific,isc:%s' % prime_colors[index]
    else:
        url += '&start=' + str(index+1)

    request = urllib2.Request(url, None)
    return request

def average_color(img):
    img_hist = img.histogram()
    r = img_hist[0:256]
    g = img_hist[256:512]
    b = img_hist[512:768]
    nPixels = img.size[0]*img.size[1]
    avg_r = sum(i*w for i,w in enumerate(r)) / nPixels
    avg_g = sum(i*w for i,w in enumerate(g)) / nPixels
    avg_b = sum(i*w for i,w in enumerate(b)) / nPixels
    return avg_r, avg_g, avg_b

class MainHandler(webapp2.RequestHandler):

    def get(self):
        job_id = int(self.request.get('job_id'))
        prime = int(self.request.get('prime'))
        tile_start = int(self.request.get('tile_start'))
        tile_end = int(self.request.get('tile_end'))

        if globals.VERBOSE: logging.info("TESTING #######            process_tiles MainHandler start")
        if globals.VERBOSE: logging.info("--- job_id: %d, prime: %d, tile_start: %d, tile_end: %d" % (job_id,prime,tile_start,tile_end))

        primes = [x for x in functions.primes_sieve(globals.MAX_PRIME_TASKS)]

        """
        # Wait until
        if not functions.is_done(job_id,'process_source',globals.NR_TASKS_SOURCE,primes):
            key = '%d:%s_counter' % (job_id, 'process_source')
            counter_value = memcache.get(key)
            if counter_value is None:
                if not memcache.add(key, 1, globals.MEMCACHE_TIMEOUT):
                    logging.error('processing:: source THIS IS NOT GOOD, FIX IT')
                    # TODO handle
                    return False
        """

        job = functions.get_job(job_id)


        api_key = globals.API_KEY
        if job.user_id == users.User(email='test@example.com'):
            if globals.VERBOSE: logging.info('--- well hello test@example.com we like you se here is your special api_key')
            api_key = globals.API_KEY_MARTIN
        elif job.user_id == users.User(email='cloudcomputing.testuser@gmail.com'):
            if globals.VERBOSE: logging.info('--- well hello test user we like you se here is your special api_key')
            api_key = globals.API_KEY_BEN


        if globals.COLOR_SEARCH:
            index = prime
        else:
            index = tile_start
        thumbnailURLs = get_thumbnail_urls(job.search_term, index, api_key=api_key)

        temp_tiles = []
        for item_position, thumbnailURL in enumerate(thumbnailURLs):
            #thumbnailURL = item['image']['thumbnailLink']
            data_string = urllib2.urlopen(thumbnailURL, timeout=globals.URL_TIMEOUT).read()

            img = PILImage.open(StringIO.StringIO(data_string))
            resized_img = img.resize((int(job.patch_width*job.tile_resolution_factor),
                                      int(job.patch_height*job.tile_resolution_factor)), PILImage.ANTIALIAS)
            avgR, avgG, avgB = average_color(resized_img)
            if (avgR + avgG + avgB) <= 30:
                print("Found tile with low avg: tile {0}, metric: {1} url: {2}".format(tile_start+item_position,
                                                                                    average_color(resized_img),
                                                                                    thumbnailURL))
                print("disabling this tile, something is wrong...")
                avgR = avgG = avgB = 999

            output = StringIO.StringIO()
            resized_img.save(output, format="PNG")
            tb_bytestring = output.getvalue()
            #memcache.add('tb:' + thumbnailURL, tb_bytestring)
            temp_tiles.append(datastore.Tile(index=tile_start+item_position, tb_url=thumbnailURL,
                                               metric=[avgR, avgG, avgB], bytestring=tb_bytestring))

        result = functions.create_result(job_id)
        result.tiles = temp_tiles
        result.tile_prime = prime
        result.source_prime = 0
        result.map_prime = 0
        result.put()


        while not functions.update_counter(job_id,'process_tiles',prime):
            if globals.VERBOSE: logging.info('--- update counter failed for process tiles with prime %d, retrying ...' % prime)
        if globals.VERBOSE: logging.info("TESTING #######            process_tiles MainHandler done")




app = webapp2.WSGIApplication([
    ('/process_tiles/', MainHandler),
], debug=True)


