import webapp2
import globals
import logging
import webapp2
import os
import jinja2
from google.appengine.api import users

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


class RedirectHandler(webapp2.RequestHandler):
    def get(self):
        if globals.VERBOSE: logging.info('-- redirecting')

        user_id = None
        user = users.get_current_user()
        if user:
            user_id = user.email()
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'

        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template_values = {
            'user_id': user_id,
            'url': url,
            'url_linktext': url_linktext,
        }

        template = JINJA_ENVIRONMENT.get_template('not_found.html')
        self.response.write(template.render(template_values))

app = webapp2.WSGIApplication([
    ('/.*', RedirectHandler),
], debug=True)
