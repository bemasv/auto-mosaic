from PIL import Image as PILImage

def average_color(img):
    img_hist = img.histogram()
    r = img_hist[0:256]
    g = img_hist[256:512]
    b = img_hist[512:768]
    nPixels = img.size[0]*img.size[1]
    avg_r = sum(i*w for i,w in enumerate(r)) / nPixels
    avg_g = sum(i*w for i,w in enumerate(g)) / nPixels
    avg_b = sum(i*w for i,w in enumerate(b)) / nPixels
    return avg_r, avg_g, avg_b



img = PILImage.open("/Users/Martin/Downloads/tb.png")
print average_color(img)