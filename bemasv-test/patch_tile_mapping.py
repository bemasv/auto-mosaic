__author__ = 'bemasv'

#
# Finds the best match of a tile to a patch.
#
# The results are appended to the patch_tile_map entry which is a list
# consisting of pairs 'patch_id : tile_index' stored in the Job(ndb.Model) class
#
# Input:
#   job_key - datastore key of the current job
#   y_patch_start - the y-coordinate (in patches) of the patch to start processing
#   y_patch_end - the y-coordinate (in patches) of the patch to process to
#

import globals
import datastore
import webapp2
import time
import functions
import urllib
from google.appengine.api import taskqueue
import logging

class WaitHandler(webapp2.RequestHandler):

    def get(self):
        job_id = int(self.request.get('job_id'))
        prime = int(self.request.get('prime'))
        patch_start = int(self.request.get('patch_start'))
        patch_end = int(self.request.get('patch_end'))


        if globals.VERBOSE: logging.info("TESTING #######            patch_tile_mapping WaitHandler start")
        if globals.VERBOSE: logging.info("--- job_id: %d, patch_start: %d, patch_end: %d" % (job_id, patch_start, patch_end))

        if globals.SERVER:
            primes = functions.primes_sieve(globals.MAX_PRIME_TASKS)
            ## WAIT UNTIL process source and tiles are COMPLETED
            functions.wait_until_done(job_id, 'process_source', globals.NR_TASKS_SOURCE, primes)
            functions.wait_until_done(job_id, 'process_tiles', globals.NR_TASKS_TILES, primes)

        if globals.VERBOSE: logging.info("waiting done ready to launch mapping, sleeping %d secs to avoid problems" % globals.SAFE_MARGIN)
        time.sleep(globals.SAFE_MARGIN)


        query_params = {'job_id': job_id,
                                'prime': prime,
                                'patch_start': patch_start,
                                'patch_end': patch_end}
        taskqueue.Task(url='/patch_tile_mapping/', params=query_params, method='GET').add(queue_name='mapping-queue')

        if globals.VERBOSE: logging.info("TESTING #######            patch_tile_mapping WaitHandler end")

class MainHandler(webapp2.RequestHandler):

    def get(self):
        job_id = int(self.request.get('job_id'))
        prime = int(self.request.get('prime'))
        patch_start = int(self.request.get('patch_start'))
        patch_end = int(self.request.get('patch_end'))

        if globals.VERBOSE: logging.info("TESTING #######            patch_tile_mapping MainHandler start")
        if globals.VERBOSE: logging.info("--- job_id: %d, patch_start: %d, patch_end: %d" % (job_id, patch_start, patch_end))

        power = 4
        result_tiles = functions.get_tiles(job_id)
        if len(result_tiles) == 0:
            logging.error('could not retrieve result_tiles for mapping')
        tiles = []
        for result_tile in result_tiles:
            for tile in result_tile.tiles:
                tiles.append(tile)
        result_patch = functions.get_patch(job_id,prime)
        if result_patch is None:
            logging.error('could not retrieve result_patch for mapping')
        patches = result_patch.patches
        for patch in patches:
            # Find best match -> tile_index
            p_red = patch.metric[0]
            p_green = patch.metric[1]
            p_blue = patch.metric[2]
            dist = [42000] * len(tiles)
            for tile in tiles:
                if tile.index >= len(tiles):
                    logging.error('mapping:: The tile.index is larger than the total number of tiles.')
                t_red = tile.metric[0]
                t_green = tile.metric[1]
                t_blue = tile.metric[2]
                dist[tile.index] = (pow(abs(p_red-t_red), power) +
                                    pow(abs(p_blue-t_blue), power) +
                                    pow(abs(p_green-t_green), power))
            if len(dist) == 0:
                logging.error('mapping:: No distances were calculated!')
            patch.tile_index = dist.index(min(dist))


        result_patch.map_prime = result_patch.source_prime
        result_patch.put()

        while not functions.update_counter(job_id,'mapping',prime):
            if globals.VERBOSE: logging.info('--- update counter failed for mapping with prime %d, retrying ...' % prime)

        if globals.VERBOSE: logging.info("TESTING #######            patch_tile_mapping MainHandler done")


app = webapp2.WSGIApplication([
    ('/patch_tile_mapping/', MainHandler),
    ('/patch_tile_mapping/wait/', WaitHandler),
], debug=True)
