__author__ = 'bemasv'

#
# Constructs the mosaic image based on the results in 'job_id'_patch_tile_mapping
# stored in memcache.
#
# The resulting image is uploaded to the blobstore and the serving url saved to the job in the datastore.
#
# Input:
# job_id - datastore key of the current job
#   result_id
#

import globals
import datastore
import logging
import functions
import webapp2
import os
import time

import urllib2
import urllib

from google.appengine.api import images as GAEimages
import cloudstorage as gcs
from google.appengine.api import (blobstore, memcache, taskqueue)
from google.appengine.ext import ndb

gcs_default_retry_params = gcs.RetryParams(initial_delay=0.2,
                                           max_delay=5.0,
                                           backoff_factor=2,
                                           max_retry_period=15)
gcs.set_default_retry_params(gcs_default_retry_params)

# TODO get tiles and patches from memcache rather than datastore?
def get_tile_bytestrings():
    pass


class CleanUpWaitHandler(webapp2.RequestHandler):
    def get(self):
        job_id = int(self.request.get('job_id'))

        if globals.SERVER:
            ready = False
            key = '%d:completed' % job_id
            while not ready:
                status = memcache.get(key)
                if status == globals.JOB_COMPLETED:
                    if globals.VERBOSE: logging.info('clean up wait done, proceeding to clean up')
                    ready = True
                else:
                     time.sleep(globals.WAITING_STEP)

        query_params = {'job_id': job_id}
        taskqueue.Task(url='/construct_image/clean_up/', params=query_params, method='GET').add(queue_name='construct-queue')



class CleanUpHandler(webapp2.RequestHandler):
    # TODO: Clean up region files
    def get(self):
        job_id = int(self.request.get('job_id'))

        if not globals.CLEAN_UP_RESULTS:
            if globals.VERBOSE: logging.info("TESTING #######            clean up not in effect")
        else:
            if globals.VERBOSE: logging.info("TESTING #######            clean up start")

            # Delete result entry from datastore
            qry = datastore.Result.query(ancestor=datastore.result_key('Job' + str(job_id)))
            results = qry.fetch()

            blob_keys = []
            if results is not None:
                for result in results:
                    if globals.VERBOSE: logging.info('--- deleting result with key: %s' % result.key)
                    blob_keys.append(result.region_blob_key)
                    ndb.delete_multi([result.key])

            blobstore.delete(blob_keys=blob_keys)

        if globals.VERBOSE: logging.info("TESTING #######            clean up end")

class WaitHandler(webapp2.RequestHandler):
    def get(self):
        job_id = int(self.request.get('job_id'))

        if globals.VERBOSE: logging.info("TESTING #######            construct_image WaitHandler start")
        if globals.VERBOSE: logging.info("--- job_id: %d" % (job_id))

        if globals.SERVER:
            primes = functions.primes_sieve(globals.MAX_PRIME_TASKS)
            functions.wait_until_done(job_id, 'region_constructing', globals.NR_TASKS_SUBCONSTRUCT, primes)

        if globals.VERBOSE: logging.info("--- done waiting for region_constructing ready to launch final construct, sleeping %d secs to avoid problems" % globals.SAFE_MARGIN)
        time.sleep(globals.SAFE_MARGIN)

        query_params = {'job_id': job_id}
        taskqueue.Task(url='/construct_image/', params=query_params, method='GET').add(queue_name='construct-queue')

        if globals.VERBOSE: logging.info("TESTING #######            construct_image WaitHandler end")


class RegionWaitHandler(webapp2.RequestHandler):
    def get(self):
        job_id = int(self.request.get('job_id'))
        prime = int(self.request.get('prime'))
        patch_start = int(self.request.get('patch_start'))
        patch_end = int(self.request.get('patch_end'))

        if globals.VERBOSE: logging.info("TESTING #######            construct_image RegionWaitHandler start")
        if globals.VERBOSE: logging.info("--- job_id: %d" % (job_id))

        if globals.SERVER:
            primes = functions.primes_sieve(globals.MAX_PRIME_TASKS)
            functions.wait_until_done(job_id, 'mapping', globals.NR_TASKS_MAPPING, primes)

        if globals.VERBOSE: logging.info("--- done waiting for mapping ready to launch construct, sleeping %d secs to avoid problems" % globals.SAFE_MARGIN)
        time.sleep(globals.SAFE_MARGIN)

        query_params = {'job_id': job_id,
                        'prime': prime,
                        'patch_start': patch_start,
                        'patch_end': patch_end}
        taskqueue.Task(url='/construct_image/region/', params=query_params, method='GET').add(queue_name='construct-queue')

        if globals.VERBOSE: logging.info("TESTING #######            construct_image RegionWaitHandler end")


class MainHandler(webapp2.RequestHandler):
    def get(self):
        job_id = int(self.request.get('job_id'))

        job = functions.get_job(job_id)

        tile_width = job.tile_resolution_factor*job.patch_width
        tile_height = job.tile_resolution_factor*job.patch_height
        total_width = int(tile_width*job.nr_patches_wide)
        total_height = int(tile_height*job.nr_patches_high)

        composite_specs = []
        composite_counter = 0
        results = functions.get_region(job_id)
        if globals.VERBOSE: logging.info("Number of mosaic regions: %d" % len(results))

        for result in results:
            region_image = GAEimages.Image(blob_key=result.region_blob_key)
            composite_specs.append((region_image, 0,0, 1.0, GAEimages.TOP_LEFT))
            composite_counter += 1
            if composite_counter >= 15:
                composite = GAEimages.composite(composite_specs, width=total_width, height=total_height,
                                    output_encoding=GAEimages.PNG)
                composite_specs = [(GAEimages.Image(image_data=composite), 0, 0, 1.0, GAEimages.TOP_LEFT)]
                composite_counter = 0

        overlay = GAEimages.Image(blob_key=job.source_blob_key)
        overlay.resize(width=total_width,height=total_height)
        overlay = overlay.execute_transforms(output_encoding=GAEimages.PNG)
        alpha = 0.2

        composite_specs.append((overlay, 0, 0, alpha, GAEimages.TOP_LEFT))
        composite = GAEimages.composite(composite_specs, width=total_width, height=total_height,
                                        output_encoding=GAEimages.PNG)


        mosaic = GAEimages.Image(image_data=composite)
        bucket = '/' + globals.GCS_BUCKET_NAME
        filename = bucket + '/mosaic_%d' % (job_id)
        if globals.VERBOSE: logging.debug('\nbucket: %s\nfile: %s' % (bucket,filename))
        try:
            job.mosaic_blob_key = self.create_image_file(filename, mosaic._image_data)
            job.mosaic_url = GAEimages.get_serving_url(job.mosaic_blob_key, secure_url=None)
        except Exception, e:
            logging.exception(e)
            self.response.write('\n\nThere was an error running the gcs! '
                                'Please check the logs for more details.\n')
            # TODO HANDLE
        else:
            if globals.VERBOSE: logging.info('--- gcs mosaic creation ok')


        # Set the job to completed in memcache
        key = '%d:completed' % job_id
        if not memcache.set(key, True, globals.MEMCACHE_TIMEOUT):
            logging.exception('there should be a job key in memcache but I will add one')
            while not memcache.add(key, True, globals.MEMCACHE_TIMEOUT):
                logging.exception('--- could not add one either, retrying...')

        job.status = globals.JOB_COMPLETED
        logging.info('job: %d completed' % job_id)
        job.put()

        if globals.VERBOSE: logging.info("TESTING #######            construct_image MainHandler done")

    def create_image_file(self, filename, image):
        """Create a GCS file with GCS client lib
        Args:
          filename: GCS filename.

        Returns:
          The corresponding string blobkey for this GCS file.
        """
        self.response.write('Creating file %s\n' % filename)

        # Create a GCS file with GCS client.
        write_retry_params = gcs.RetryParams(backoff_factor=1.1)
        with gcs.open(filename,
                      'w',
                      content_type='image/png',
                      #options={'x-goog-meta-foo': 'foo',
                      #         'x-goog-meta-bar': 'bar'},
                      retry_params=write_retry_params) as f:
            f.write(image)

        # Blobstore API requires extra /gs to distinguish against blobstore files.
        blobstore_filename = '/gs' + filename
        # This blob_key works with blobstore APIs that do not expect a
        # corresponding BlobInfo in datastore.
        return blobstore.BlobKey(blobstore.create_gs_key(blobstore_filename))



class SubHandler(webapp2.RequestHandler):
    def get(self):
        job_id = int(self.request.get('job_id'))
        prime = int(self.request.get('prime'))
        patch_start = int(self.request.get('patch_start'))
        patch_end = int(self.request.get('patch_end'))


        if globals.VERBOSE: logging.info("TESTING #######            construct_image SubHandler start")
        if globals.VERBOSE: logging.info("--- job_id: %dprime: %d" % (job_id, prime))

        job = functions.get_job(job_id)


        tile_width = int(job.patch_width*job.tile_resolution_factor)
        tile_height = int(job.patch_height*job.tile_resolution_factor)

        primes = functions.get_primes(globals.MAX_PRIME_TASKS)
        position = -1
        for index, prime_val in enumerate(primes):
            if prime_val == prime:
                position = index
                break
        if position == -1:
            logging.error('Prime for job (%d) not found in prime list. Why?' % prime)


        result_patches = []
        for i in range(globals.NR_MAP_TASKS_PER_SUBCONSTRUCT):
            result_patches.append(functions.get_map(job_id,primes[position*globals.NR_MAP_TASKS_PER_SUBCONSTRUCT + i]))

        # Generate ordered list of tiles with values of bytestring
        result_tiles = functions.get_tiles(job_id) #
        tiles = [''] * (10*globals.NR_TASKS_TILES) # 10 tiles from each search API call
        for result_tile in result_tiles:
            for tile in result_tile.tiles:
                tiles[tile.index] = tile.bytestring

        composite_specs = []

        total_width = tile_width*job.nr_patches_wide
        total_height = tile_height*job.nr_patches_high

        composite_counter = 0
        #sorted_patches = sorted(result.patches,key=lambda patch: patch.index)
        for result_patch in result_patches:
            for patch in result_patch.patches:
                if patch_start <= patch.index < patch_end or True:
                    (row, col) = divmod(patch.index,job.nr_patches_wide)
                    index = patch.tile_index
                    composite_specs.append((GAEimages.Image(image_data=tiles[index]), col*tile_width, row*tile_height, 1.0, GAEimages.TOP_LEFT))
                    composite_counter += 1
                    if composite_counter >= 15:
                        composite = GAEimages.composite(composite_specs, width=total_width, height=total_height,
                                            output_encoding=GAEimages.PNG)
                        composite_specs = [(GAEimages.Image(image_data=composite), 0, 0, 1.0, GAEimages.TOP_LEFT)]
                        composite_counter = 0
            if globals.VERBOSE: logging.info('completed a patch set of %d in total' % globals.NR_TASKS_MAPPING)

        composite = GAEimages.composite(composite_specs, width=total_width, height=total_height,
                                        output_encoding=GAEimages.PNG)
        mosaic_region = GAEimages.Image(image_data=composite)
        if globals.VERBOSE: logging.info("Mosaic region start: %d, end: %d) " % (patch_start, patch_end))

        result = functions.create_result(job_id)
        result.region_prime = prime
        result.tile_prime = 0
        result.map_prime = 0
        result.source_prime = 0


        bucket = '/' + globals.GCS_BUCKET_NAME
        filename = bucket + '/mosaic_region_%d_%d' % (job_id, prime)
        if globals.VERBOSE: logging.debug('\nbucket: %s\nfile: %s' % (bucket,filename))
        try:
            result.region_blob_key  = self.create_image_file(filename, mosaic_region._image_data)
            logging.info("Region url: %s" % (GAEimages.get_serving_url(result.region_blob_key, secure_url=None)))
        except Exception, e:
            logging.exception(e)
            self.response.write('\n\nThere was an error running the gcs! '
                                'Please check the logs for more details.\n')
            # TODO HANDLE
        else:
            if globals.VERBOSE: logging.info('--- gcs mosaic creation ok')

        while not functions.update_counter(job_id,'region_constructing',prime):
            if globals.VERBOSE: logging.info( '--- update counter failed for construct_image w prime %d, retrying...' % prime)

        result.put()

    def create_image_file(self, filename, image):
        """Create a GCS file with GCS client lib
        Args:
          filename: GCS filename.

        Returns:
          The corresponding string blobkey for this GCS file.
        """
        self.response.write('Creating file %s\n' % filename)

        # Create a GCS file with GCS client.
        write_retry_params = gcs.RetryParams(backoff_factor=1.1)
        with gcs.open(filename,
                      'w',
                      content_type='image/png',
                      #options={'x-goog-meta-foo': 'foo',
                      #         'x-goog-meta-bar': 'bar'},
                      retry_params=write_retry_params) as f:
            f.write(image)

        # Blobstore API requires extra /gs to distinguish against blobstore files.
        blobstore_filename = '/gs' + filename
        # This blob_key works with blobstore APIs that do not expect a
        # corresponding BlobInfo in datastore.
        return blobstore.BlobKey(blobstore.create_gs_key(blobstore_filename))



app = webapp2.WSGIApplication([
                                  ('/construct_image/', MainHandler),
                                  ('/construct_image/region/', SubHandler),
                                  ('/construct_image/region/wait/', RegionWaitHandler),
                                  ('/construct_image/wait/', WaitHandler),
                                  ('/construct_image/clean_up/', CleanUpHandler),
                                  ('/construct_image/clean_up/wait/', CleanUpWaitHandler),
                              ], debug=True)
