__author__ = 'bemasv'

#
# Processes the source image and calculate the metrics for the patches.
#
#
# The calculated metrics are appended to 'job_id'_metrics in memcache which is
# a list of pairs with 'patch_id : metric'
#
# Input:
#   job_key - datastore key of the current job
#   y_patch_start - the y-coordinate (in patches) of the patch to start processing
#   y_patch_end - the y-coordinate (in patches) of the patch to process to
#

import globals
import datastore
import functions
import logging
from google.appengine.api import memcache
from google.appengine.ext import blobstore
from PIL import Image
import PIL

import urllib
import webapp2
import time
from math import floor, ceil

from google.appengine.ext import ndb

def average_color(img):
    img_hist = img.histogram()
    r = img_hist[0:256]
    g = img_hist[256:512]
    b = img_hist[512:768]
    nPixels = img.size[0]*img.size[1]
    avg_r = sum(i*w for i,w in enumerate(r)) / nPixels
    avg_g = sum(i*w for i,w in enumerate(g)) / nPixels
    avg_b = sum(i*w for i,w in enumerate(b)) / nPixels
    return avg_r, avg_g, avg_b


class MainHandler(webapp2.RequestHandler):

    def get(self):
        job_id = int(self.request.get('job_id'))
        prime = int(self.request.get('prime'))
        y_start = int(self.request.get('y_start'))
        y_end = int(self.request.get('y_end'))

        if globals.VERBOSE: logging.info("TESTING #######            process_source MainHandler start")
        if globals.VERBOSE: logging.info("--- job_id: %d, patch_start: %d, patch_end: %d" % (job_id,y_start,y_end))

        job = functions.get_job(job_id)

        # Open image and get dimensions
        source_reader = blobstore.BlobReader(job.source_blob_key)
        img = Image.open(source_reader)
        if img.mode != 'RGB':
            img.convert('RGB')
        src_width = img.size[0]
        src_height = img.size[1]

        # Get patch dimensions from job
        patch_width = job.patch_width
        patch_height = job.patch_height

        # Sort out image dimension divisibility
        n_horizontal_patches = src_width/patch_width
        col_offset = int(ceil((src_width % patch_width)/2.0))
        row_offset = int(ceil((src_height % patch_height)/2.0))

        tmp_patches = []
        for y in range(y_start, y_end): # y = patch index
            (patch_row, patch_col) = divmod(y,n_horizontal_patches)
            row_start = patch_row*patch_height + row_offset
            row_end = row_start + patch_height - 1
            col_start = patch_col*patch_width + col_offset
            col_end = col_start + patch_width - 1

            img_patch = img.crop((col_start, row_start, col_end, row_end))
            avgR,avgG,avgB = average_color(img_patch)
            tmp_patches.append(datastore.Patch(index=y, metric=[avgR,avgG,avgB]))

        result = functions.create_result(job_id)
        result.patches = tmp_patches
        result.source_prime = prime
        result.tile_prime = 0
        result.map_prime = 0
        result.put()


        while not functions.update_counter(job_id,'process_source',prime):
            if globals.VERBOSE: logging.info( '--- update counter failed for process_source w prime %d, retrying...' % prime)

        if globals.VERBOSE: logging.info("TESTING #######            process_source MainHandler done")


app = webapp2.WSGIApplication([
    ('/process_source/', MainHandler),
], debug=True)
