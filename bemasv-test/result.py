__author__ = 'Martin'

import os
import webapp2
import jinja2
import datastore
import globals
import functions

from google.appengine.ext import ndb
from google.appengine.api import memcache
from google.appengine.ext import blobstore
from google.appengine.api import users
from google.appengine.api import images as GAEimages
import logging

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class ProgressHandler(webapp2.RequestHandler):

    def get(self):
        job_id = int(self.request.get('job_id'))

        key = '%d:completed' % job_id
        status = memcache.get(key)
        if status == globals.JOB_COMPLETED:
            job = functions.get_job(job_id)
            self.response.out.write('Completed?%s' % job.mosaic_url)
        else:
            self.response.out.write('Hold on, currently processing...')


class MainHandler(webapp2.RequestHandler):

    def get(self):
        job_id = int(self.request.get('job_id'))

        if globals.VERBOSE: logging.info("TESTING #######            result MainHandler start")
        if globals.VERBOSE: logging.info("--- job_id: %d" % job_id)

        job = functions.get_job(job_id)

        user_id = None
        user = users.get_current_user()
        if user:
            user_id = user.email()
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'

        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template_values = {
            'user_id': user_id,
            'job_id': job_id,
            'job': job,
            'url': url,
            'url_linktext': url_linktext,
        }
        template = JINJA_ENVIRONMENT.get_template('result.html')
        self.response.write(template.render(template_values))

        if globals.VERBOSE: logging.info("TESTING #######            result MainHandler end")


app = webapp2.WSGIApplication([
    ('/result/', MainHandler),
    ('/result/check_progress/', ProgressHandler),
], debug=True)
