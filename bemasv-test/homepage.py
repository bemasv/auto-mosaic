#!/usr/bin/env python
#
# The homepage of the app.
#


import os
import urllib
import webapp2
import jinja2
import globals
import datastore
import functions
import pickle
import logging

from google.appengine.ext import ndb
from google.appengine.api import taskqueue
from google.appengine.ext import blobstore
from google.appengine.api import users
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.api import images as GAEimages


import time

current_milli_time = lambda: int(round(time.time() * 1000))

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class RedirectHandler(webapp2.RequestHandler):
    def get(self):
        self.redirect('/index.html')

class MainHandler(webapp2.RequestHandler):

    def get(self):

        upload_url = blobstore.create_upload_url('/upload/task')

        user_id = None
        user = users.get_current_user()
        if user:
            user_id = user.email()
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'

        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template_values = {
            'upload_url' : upload_url,
            'user_id': user_id,
            'url': url,
            'url_linktext': url_linktext,
        }

        template = JINJA_ENVIRONMENT.get_template('upload.html')
        self.response.write(template.render(template_values))


class TaskHandler(blobstore_handlers.BlobstoreUploadHandler):

    def post(self):
        job = functions.create_job()
        if users.get_current_user():
            job.user_id = users.get_current_user()
            logging.info('--- current user: %s' % job.user_id)
        else:
            job.user_id = None

        job.search_term = self.request.get('search_term')
        upload_files = self.get_uploads('source_img')
        if upload_files:
            blob_info = upload_files[0]
            job.source_blob_key = blob_info.key()
            job.source_url = GAEimages.get_serving_url(job.source_blob_key, secure_url=None)
        else:
            # TODO Throw exception and handle it
            print 'ERROR'

        job.status = globals.JOB_INIT
        job_id = job.put().id()

        query_params = {'job_id': job_id}
        self.redirect('/processing/?' + urllib.urlencode(query_params))


class ResultHandler(webapp2.RequestHandler):

    def get(self):
        job_id = self.request.get('job_id')
        job = datastore.Job.get_by_id(int(job_id), parent=datastore.job_key())

        template_values = {
            'job': job,
        }
        template = JINJA_ENVIRONMENT.get_template('result.html')
        self.response.write(template.render(template_values))

class UserHandler(webapp2.RequestHandler):

    def get(self):
        job_number = 18
        if self.request.get('job_number'):
            job_number = int(self.request.get('job_number'))
        if job_number<1:
            job_number = 18
        user = users.get_current_user()
        user_jobs = []
        user_id = None

        if user:
            user_id = user.email()
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
            qry = datastore.Job.query(ndb.AND(datastore.Job.user_id == user,
                                          datastore.Job.status == globals.JOB_COMPLETED))
            qry = qry.order(-datastore.Job.date)
            user_jobs = qry.fetch(job_number)

        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template_values = {
            'user_jobs': user_jobs,
            'user_id': user_id,
            'url': url,
            'url_linktext': url_linktext,
            'job_number' : len(user_jobs)
        }

        template = JINJA_ENVIRONMENT.get_template('userpage.html')
        self.response.write(template.render(template_values))



class IndexHandler(webapp2.RequestHandler):
 def get(self):
        qry = datastore.Job.query(ndb.AND(datastore.Job.status == globals.JOB_COMPLETED))
        qry = qry.order(-datastore.Job.date)
        jobs = qry.fetch(18)

        user = users.get_current_user()
        user_id = None

        if user:
            user_id = user.email()
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'


        template_values = {
            'user_id': user_id,
            'url': url,
            'url_linktext': url_linktext,
            'jobs': jobs,

        }
        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(template_values))


app = webapp2.WSGIApplication([
    ('/', RedirectHandler),
    ('/index.html', IndexHandler),
    ('/upload/', MainHandler),
    ('/upload/task', TaskHandler),
    ('/main/result/?', ResultHandler),
    ('/main/user/?', UserHandler),
], debug=True)
