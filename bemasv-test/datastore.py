__author__ = 'Martin'

#
# Defines the datastore classes

import globals
from google.appengine.ext import ndb

class Job(ndb.Model):
    """
    Models an individual Job entry with job_id, user
    searchterm, status, source_url, mosaic_url and date.
    All entries belong to the same entity group
    """
    # User id if logged in
    user_id = ndb.UserProperty(required=False, indexed=True)
    name = ndb.StringProperty(required=True, default='Mosaic name', indexed=False)
    description = ndb.StringProperty(required=False, indexed=False)
    # The search term entered by the user
    search_term = ndb.StringProperty(required=True, indexed=False)
    # String describing the current status: INIT, COMPLETED, ERROR
    status = ndb.IntegerProperty(required=True, indexed=True)
    # Serving url in the blobstore of the source image
    source_url = ndb.StringProperty(required=True, indexed=False)
    source_blob_key = ndb.BlobKeyProperty(required=True, indexed=False)
    # Serving url in the blobstore for the resulting image
    mosaic_url = ndb.StringProperty(required=False, indexed=False)
    mosaic_blob_key = ndb.BlobKeyProperty(required=False, indexed=False)
    # Patch dimensions for source processing
    patch_width = ndb.IntegerProperty(required=False, indexed=False)
    patch_height = ndb.IntegerProperty(required=False, indexed=False)
    nr_patches_wide = ndb.IntegerProperty(required=False, indexed=False)
    nr_patches_high = ndb.IntegerProperty(required=False, indexed=False)
    tile_resolution_factor = ndb.FloatProperty(required=True, indexed=False, default=globals.TILE_RESOLUTION_FACTOR)
    date = ndb.DateTimeProperty(auto_now_add=True)


class Tile(ndb.Model):
    """
    Models an individual Tile entry with its index, metric, thumbnail url
    for the original image, and a cache key for the processed image
    Parent should be its Result entry
    """
    index = ndb.IntegerProperty(required=True, indexed=False) # May not need indexing if we read all tiles every time
    metric = ndb.FloatProperty(required=False, repeated=True, indexed=False)
    tb_url = ndb.StringProperty(required=True, indexed=True)
    bytestring = ndb.BlobProperty(required=False, indexed=False)


class Patch(ndb.Model):
    """
    Models an individual Patch entry with its index and metric
    Parent should be its Result entry
    """
    index = ndb.IntegerProperty(required=True, indexed=True) # May not need indexing if we read all tiles every time
    metric = ndb.FloatProperty(repeated=True, indexed=False)
    tile_index = ndb.IntegerProperty(required=False, indexed=False, default=-1)
    

class Result(ndb.Model):
    """
    Models the result for one job, each entry should belong to its own entity group
    """
    #job_key = ndb.KeyProperty(kind=Job, required=True, indexed=True)
    tiles = ndb.LocalStructuredProperty(Tile, repeated=True)
    patches = ndb.LocalStructuredProperty(Patch, repeated=True)
    region_blob_key = ndb.BlobKeyProperty(required=False, indexed=False)
    tile_prime = ndb.IntegerProperty(required=True, indexed=True) # Set to 0 if not used
    source_prime = ndb.IntegerProperty(required=True, indexed=True) # Set to 0 if not used
    map_prime = ndb.IntegerProperty(required=True, indexed=True) # Set to 0 if not used
    region_prime = ndb.IntegerProperty(required=False, indexed=True) # Set to 0 if not used

class SearchTermResults(ndb.Model):
    """ Models the results of a call to the image search API, storing the URL
    of the thumbnail images that are returned
    """
    pickled_thumbnail_url_list = ndb.PickleProperty(required=True, indexed=False)
    date = ndb.DateTimeProperty(auto_now_add=True)


def job_key(job_ancestor=globals.JOB_ANCESTOR):
    """Constructs a Datastore key for a Job entity with JOB_ANCESTOR."""
    return ndb.Key('Job', job_ancestor)

def result_key(result_ancestor):
    """Constructs a Datastore key for a result entity."""
    return ndb.Key('Result', result_ancestor)


