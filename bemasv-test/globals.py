__author__ = 'Martin'

import os
from google.appengine.api import app_identity
import logging
import math

DEV = False
SERVER = True

if SERVER:
    VERBOSE = False
    CLEAN_UP_RESULTS = True
else:
    VERBOSE = True
    CLEAN_UP_RESULTS = False

# Enumerations used for job.status
JOB_INIT = 0
JOB_COMPLETED = 1

# Perform search for each significant color (12)
COLOR_SEARCH = False

## Processing parameters
NR_TASKS_SOURCE = 15
if COLOR_SEARCH:
    NR_TASKS_TILES = 8 #10 DEBUG
else:
    NR_TASKS_TILES = 8  #10 DEBUG
NR_TASKS_MAPPING = NR_TASKS_SOURCE
NR_TASKS_SUBCONSTRUCT = int(math.ceil(NR_TASKS_MAPPING/1))
NR_MAP_TASKS_PER_SUBCONSTRUCT = NR_TASKS_MAPPING/NR_TASKS_SUBCONSTRUCT
MAX_PRIME_TASKS = 100  # -> 25 primes below 100 Needs to be at least x number of primes below this value, where x=max nr of tasks

## Timing/waiting parameters
URL_TIMEOUT = 40

SAFE_MARGIN = 0.5 # Extra seconds to wait after waiting
WAITING_STEP = 2 # Seconds to wait until check again if process completed
WAIT_LAUNCH = 0  # Seconds to wait between starting tasks
WAIT_TIMEOUT = 300  # Seconds before trying even if counters are not done
CHECK_DS_TIMEOUT = 60 # Seconds before checking datastore vs memcache
MAX_MEMCACHE_RETRIES = 5

## Image parameters
# Resolution of each individual tile (square aspect ratio)
TILE_RESOLUTION_FACTOR = 2.8
# min number of patches in any dimension
MIN_NUMBER_OF_PATCHES = 20 #20 DEBUG
MAX_NUMBER_OF_PATCHES = 80
# max patch size.
# There will be more than MIN_NUMBER_OF_PATCHES whe img dimension > MIN_NUMBER_OF_PATCHES*MAX_PATCH_SIZE.
MAX_PATCH_SIZE = 24 #16 DEBUG
#float,  Max dimension in pixel value of resulting image
MAX_DIMENSION = 4000.0
# composite function can't handle values larger than 4000.0
if MAX_DIMENSION > 4000.0:
    MAX_DIMENSION = 4000.0


# Storage parameters
MEMCACHE_TIMEOUT = 600
JOB_ANCESTOR = 'jobs'
COUNTER_ANCESTOR = 'counters'


## GAE and API parameters
API_KEY = 'AIzaSyBFxg2o7hPQDPU6Bt8sBp6FcstUhQiQW3Y' # 'AIzaSyClC-lWhWYhbwlvtN9woqKlY3aD16JHKec'
API_KEY_OLD = 'AIzaSyBsg4ly7HwrQeN6xt5wGdaXKK-m8IKQNjM' # server
API_KEY_BROWSER_OLD =  'AIzaSyABNJLtg6ozpzfcr7h-m8lm9hlBovMj34o' # browser
SEARCH_ENGINE_ID = '012244892872980226684:i5bg9u6hvgu'

API_KEY_SVEN = 'AIzaSyB58wvcuSS5DtSuse897MFNY1h_0n2cGvk'  # Sven's key
API_KEY_MARTIN = 'AIzaSyBFxg2o7hPQDPU6Bt8sBp6FcstUhQiQW3Y'  # Martin's key
API_KEY_BEN = 'AIzaSyAhiZeKY5NfjVN1pTZBkj-UxLW0t7IVmKQ' # Ben's Key

GCS_BUCKET_NAME = app_identity.get_default_gcs_bucket_name()
if SERVER:
    GCS_BUCKET_NAME = 'momomosaic-uob.appspot.com'


# Using this as a notepad
# TODO future work
"""
Martin
* upload to gae, DONE
* user test page, DONE
* user support, e.g. set API key, DONE
* search only for color images -> search for specific colors, DONE
* delete tasks if not completed within 20 min, DONE


Ben
* construct image with sub tasks, DONE

Sven
* DESGIN
* user/profile page

TODO
* serve full scale images
* stability needed!
* Flickr integration
* handle errors, e.g. change job status to error
* can handle about 500 tiles in one process_source task
* counter updates are sometimes missed / not properly distributed over the database
* maybe not have tb urls in memcache, reserve space for more valuable info such as counters?



"""