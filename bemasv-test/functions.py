__author__ = 'Martin'

import globals
import datastore
import time
import logging
import urllib2
import StringIO
from google.appengine.api import memcache
from google.appengine.ext import ndb

def update_counter(job_id, task_name, prime):
    key = '%d:%s_counter' % (job_id, task_name)
    client = memcache.Client()
    num_retries = 0
    while num_retries < globals.MAX_MEMCACHE_RETRIES: # Retry loop
        counter = client.gets(key)
        if counter is None:
            logging.exception('counter value not set, setting: %s to 1' % key)
            client.cas(key, 1)
        elif client.cas(key, counter*prime):
            break
        else:
            num_retries += 1

    return num_retries < globals.MAX_MEMCACHE_RETRIES


def primes_sieve(limit):
    """
    a = [True] * limit
    a[0] = a[1] = False

    for (i, isprime) in enumerate(a):
        if isprime:
            yield i
            for n in xrange(i*i, limit, i):     # Mark factors non-prime
                a[n] = False
    """
    return get_primes(limit)


def get_primes(limit):
    primes = []
    for p in range(2,int(limit)+1):
        for i in range(2,p):
            if p%i == 0:
                break    # <== break here (when a factor is found)
        else:            # <==else belongs to the for, not the if
            primes.append(p)
    return primes



def wait_until_done(job_id, task_name, nr_tasks, primes):
    process_done = False
    start_time = time.time()
    while not process_done:
        process_done = is_done(job_id,task_name,nr_tasks,primes)
        if time.time() - start_time > globals.CHECK_DS_TIMEOUT:
            validate_memcache_against_ds(job_id, task_name, nr_tasks, primes)
        if time.time() - start_time > globals.WAIT_TIMEOUT:
            logging.exception('waiting for %s timed out, continuing' % task_name)
            break
        else:
            time.sleep(globals.WAITING_STEP)
    return

def is_done(job_id, task_name, nr_tasks, primes):
    done = False
    key = '%d:%s_counter' % (job_id, task_name)
    counter_value = memcache.get(key)
    if counter_value is not None:
        for i in range(nr_tasks):
            if counter_value % primes[i] != 0:
                break
            if i == nr_tasks-1:
                done = True
    else:
        logging.exception('could not find memcache key %s, adding it with value 1' % key)
        if not memcache.add(key, 1, globals.MEMCACHE_TIMEOUT):
            logging.exception('Memcache key %s for counter in %s could not be added' % (key,task_name))

    return done

def validate_memcache_against_ds(job_id, task_name, nr_tasks, primes):
    # TODO FINISH THIS
    '''
    unfinished_tasks = find_unfinished_tasks(job_id, task_name, nr_tasks, primes)
    for task_prime in unfinished_tasks:
    '''
    return True


def find_unfinished_tasks(job_id, task_name, nr_tasks, primes):
    unfinished_tasks = []
    key = '%d:%s_counter' % (job_id, task_name)
    counter_value = memcache.get(key)
    if counter_value is not None:
        for i in range(nr_tasks):
            if counter_value % primes[i] != 0:
                unfinished_tasks.append(primes[i])
    else:
        logging.exception('could not find memcache key %s, adding it with value 1' % key)
        if not memcache.add(key, 1, globals.MEMCACHE_TIMEOUT):
            logging.exception('Memcache key %s for counter in %s could not be added' % (key,task_name))

    return unfinished_tasks



def get_job(job_id):
    return datastore.Job.get_by_id(job_id, parent=datastore.job_key())

def get_result(job_id, result_id):
    return datastore.Result.get_by_id(result_id, parent=datastore.result_key('Job'+str(job_id)))

def get_tiles(job_id):
    qry = datastore.Result.query(datastore.Result.tile_prime > 0,
                                 ancestor=datastore.result_key('Job' + str(job_id)))
    return qry.fetch()

def get_patch(job_id, prime):
    qry = datastore.Result.query(datastore.Result.source_prime == prime,
                                 ancestor=datastore.result_key('Job' + str(job_id)))
    list = qry.fetch(1)
    return list[0]

def get_map(job_id, prime):
    qry = datastore.Result.query(datastore.Result.map_prime == prime,
                                 ancestor=datastore.result_key('Job' + str(job_id)))
    list = qry.fetch(1)
    return list[0]

def get_region(job_id):
    qry = datastore.Result.query(datastore.Result.region_prime > 0,
                                 ancestor=datastore.result_key('Job' + str(job_id)))
    return qry.fetch()

def create_job():
    return datastore.Job(parent=datastore.job_key())

def create_result(job_id):
    return datastore.Result(parent=datastore.result_key('Job' + str(job_id)))

def get_thumbnail_bytestring(thumbnail_url, result_key):
    thumbnail_bytestring = memcache.get('tb:' + thumbnail_url)
    if thumbnail_bytestring is not None:
        return thumbnail_bytestring
    else:
        qry = datastore.Tile.query(ancestor=result_key)
        qry.filter(datastore.Tile.tb_url == thumbnail_url)
        tb_tile = qry.get()
        if tb_tile is None:
            print "---- get_thumbnail_bytestring: I've lost a tile! (Or I've written the query incorrectly)"
        return tb_tile.bytestring


