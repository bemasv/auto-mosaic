\documentclass[10pt, final, conference, letterpaper, twocolumn, twoside]{IEEEtran}
\usepackage[top=0.75in,bottom=1.69in,left=0.56in,right=0.56in]{geometry} % see geometry.pdf on how to lay out the page. There's lots.
\geometry{a4paper}
\usepackage{graphicx}
\usepackage{cite}
\usepackage{url}

% Date
\usepackage[yyyymmdd]{datetime}
\renewcommand{\dateseparator}{-}
\newdateformat{mydate}{\monthname[\THEMONTH] \ordinal{DAY}, \THEYEAR}

% Math
\usepackage{amsmath}
%\usepackage{mathtools}

\usepackage{listings}
\lstset{basicstyle=\small\ttfamily,breaklines=true,showstringspaces=false}

\usepackage{caption}


% Footer
%\lfoot{University of Bristol \\Cloud Computing COMSM0010}
%\cfoot{}
%\rfoot{\date}

% Title
\title{MoMoMosaic}
\author{\small Martin Andersson \\
		 Candidate No: 17725\\
		 \small University of Bristol
		 \and
	    \small Sven Hollowell \\
		 Candidate No: 17421\\
		 \small University of Bristol
		 \and
	    \small Ben Leslie \\
		 Candidate No: 17340\\
		 \small University of Bristol}
\usepackage{fancyhdr}

%%% BEGIN DOCUMENT
\begin{document}
\pagenumbering{gobble}

%	\begin{titlepage}
%		\maketitle	
%		\thispagestyle{empty} % or alph
		
%		\clearpage
%	\end{titlepage}
\pagestyle{fancy}
{\setlength{\parindent}{0.5cm}

\maketitle

\noindent
\textbf{ \emph{Abstract--} \small MoMoMosaic is a cloud computing service that provides users with custom generated photomosaics - pictures built up out of smaller pictures, or ‘tiles’. The service is hosted on Google App Engine, with a web frontend that allows user to select pictures using local files, a Google image search term, or social media sources. The application can be run online at http://momomosaic-uob.appspot.com/main}
\\

\noindent
\textbf{ \emph{Keywords--} Cloud computing, photomosaic, Google App Engine, embarrassingly parallel, image processing, social web.}

\section{Introduction}
MoMoMosaic brings new image processing techniques to user content driven social media, by allowing users to create image mosaics. In an image mosaic, a user uploads a source image to be converted to a mosaic, and a library of tile images is used to try and rebuild the source image. An example is shown in Fig. \ref{fig:example}. There exist several online image mosaic creation services\cite{web:Mosaically}\cite{web:EasyMoza}\cite{web:bighugelabs}, but all require explicit specification of tile image libraries - MoMoMosaic simplifies image mosaic creation by giving users the ability to use verbal descriptions of tile images. These verbal descriptions are fed to Google's image search algorithm to source a variety of tile images, thus bypassing the need to upload or specify a large custom library of tiles. Alternatively, users can select images from their own Facebook, Flickr or Instagram accounts. With the inputs established, the image processing itself is performed server-side. Thanks to the embarassingly parallel nature of many of the steps in our image mosaic algorithm, MoMoMosaic employs a scale-out model to cope with increased demand, thus leveraging the advantages of cloud computing infrastructure. Provided a user has logged-in to the website, the resulting image mosaic is added to a user gallery and is retrievable on future visits. Mosaics from a user's gallery can be submitted to a public gallery, where other users of the service can rate the mosaic, with the top rated public mosaics being displayed on the front page. A social media environment is created with the ability for users to `follow' other users, and so be notified of their future public submissions, as well as the ability for users to comment on one another's images.  


\section{Infrastructure vs. Platform as a Service}
There are currently two major cloud computing providers, Amazon and Google, with major companies such as Microsoft and IBM forming a part of the competition. Historically choosing between the two has been synonymous with choosing Infrastructure as a Service (IaaS) or Platform as a Structure (PaaS). Amazon have with their Amazon Web Services (AWS) \cite{AWS} been providing IaaS and Google has provided PaaS through the Google App Engine (GAE) \cite{GAE}. Recently Google has also started to offer IaaS through their Google Compute Engine \cite{compute_engine} and Amazon is offering AppStream \cite{appstream} which resemble PaaS.

IaaS essentially provides a set of virtual machines that can be accessed remotely. By doing so it gives the developer full control over what type of servers, databases, programming languages and so forth to use. It also requires the developer to configure the system properly with all of the aspects that it includes, such as database setup and scaling conditions.

Developer application software functionality is basically what PaaS is providing. Once a provider is chosen the choice of OS, database, web-server etc. is usually fixed. The advantage being that it comes preconfigured to a much larger extent than IaaS.

Given the time constraint and that GAE supports Python it was chosen in favour of AWS due to the lower demand of configuring it. Furthermore, the Google Seach API has an essential role in the application so benefits may be drawn from running the application in Google's data centres. Only GAE and AWS was considered as potential candidates because of their strong market position and extensive user space leading to well documented features as well as a large support community.

\section{System Architecture}
\label{sec:SysArch}
Applications on GAE are run on stateless virtual machines referred to as instances. These instances are sandboxed but can via the GAE environment access a wide range of storage, services and tools. Four different frontend instances with varying performance and consequently varying pricing are available.  USE BEN's VERSION

{\bf GAE instances!!}

The core of the application is the MoMoMosaic algorithm utilising several of the different storage alternatives, services and tools available through GAE as well as APIs supplied by social media websites. An overview of the system architecture is shown in figure \ref{fig:sys_arch} and the components depicted in there are discussed in further detail below. GAE is built around handling URL requests and together with algorithms implemented in Python and the components above they form the core of our application.

\begin{figure}[!ht]
	\centering
	\includegraphics[height=0.6\columnwidth]{fig/system_architecture.png}
	\caption{Overview of the system architecture for MoMoMosaic.}
	\label{fig:sys_arch}
\end{figure}

\subsection{Storage}
Three levels of storage with different properties and restrictions are in use. Memcache provides a distributed data cache with high read and write times. Data stored in Memcache is not guaranteed to be persistent and the maximum size of an entry is slightly below 1 MB \cite{memcache}. 

The NDB Datastore, only referred to as Datastore in this document, is a schemaless object datastore that does give persistent storage. It supports automatic caching, complex queries as well as nested data structures but is also limited to \emph{entities} with a maximum size of 1 MB but can hold a total of 1 GB in the free version \cite{datastore}. Each entry in the Datastore, an entity, belongs to a entity group with a common ancestor. Queries can only be performed on a limited number of entity groups but the limitation of 1 write per second also only applies per entity group. This needs to be taken into account when designing the Datastore models.

For larger data the Blobstore has to be used where the only restriction is the total amount of data stored which in the free version is 5 GB. The Blobstore provides a simple structure where data only can be written, read or deleted but it has a convenient feature where it can serve a URL giving direct access to a stored image \cite{blobstore}. Blob data are created indirectly through the use of HTTP POST requests so in order for the application to write large data files the \emph{Google Cloud Storage Client Library} was used which provides an interface enabling write operations to the \emph{Blobstore} from within the application \cite{gcs}.

\subsection{Google App Engine Services}
The GAE environment gives access to several different services of which \emph{Task Queues} is the one most heavily used by the MoMoMosaic application and also the one enabling us to benefit from some of the advantages of cloud computing. Normal HTTP requests has a response deadline of 60 seconds in GAE and can not be performed in the background. By adding tasks to one or several different queues the \emph{Task Queue} API allows for background processing initiated by user requests with a response deadline of 10 minutes. Two different types of queues can be used: push queues which are processed within the GAE based on a pre-set rate and pull queue where task consumers, residing inside or outside the GAE, can lease tasks for processing during a specific time frame. The push queue met all of our needs without the added complexity of the pull queue so the former is what have been used for implementation. \cite{taskqueue}

Three other services from the GAE envirnoment is also worth mentioning.\emph{Images} is used to handle images internally in the application. To access external URLs and retrieve the results from requests to these sites the \emph{URL Fetch} service was used. The \emph{Users} service gave an easy way of identifying users and handling authentication by letting users use their Google account on our site.

\subsection{External Libraries and APIs}
APIs from social media sites allows us to interact and use images stored by the user at those sites. The sites proposed to support are facebook, flickr and Instagram but as long as the site has a working API to retrieve images it could potentially be supported by the application. To retrieve images from based on a Google query the Google Search API is used.

Python 2.7 is the programming language used to implement the application and it makes use of a selection of modules, mentioning here is limited to the three most crucial ones. \texttt{PIL} is used for processing images, \texttt{webapp2} provides a web framework and \texttt{jinja2} is used for HTML rendering.

\section{Design \& Process Flow}
\label{sec:Design}
A Job is created when a user has uploaded a source image, the image to base the mosaic on, and specified a set of tiles, the images that will constitute the final mosaic. An overview of this naming convention of the different components is given in figure \ref{fig:components}.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.9\columnwidth]{fig/components.png}
	\caption{Overview depicting our naming convention of different components.}
	\label{fig:components}
\end{figure}

The creation of a mosaic, the Job, is divided into several stages each executed by either a single task or several running in parallel. The complete process flow is depicted in figure \ref{fig:process_flow}.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.9\columnwidth]{fig/process_flow.png}
	\caption{Process flow for creation of a mosaic.}
	\label{fig:process_flow}
\end{figure}

Before each stage can be launched the previous one has to complete. This is handled by using a unique counter for each of the stages of an individual job capable of tracking the completion of the tasks of that stage. Furthermore wait handlers are implemented that checks the status of the relevant counter and launch the next stage at the correct time. A separate Task Queue for each of the stages described below as well as one for the wait handlers enables us to initiate the whole execution at once and then let the wait handlers add tasks to the relevant queue when they are ready to be executed.

Note that the intermediate Results Job stored in the Datastore by the different stages all belong to the same entity group with. But Results form different Jobs belong to different entity group since they have their Job as ancestor. Because the need of being able to make a query across all Jobs they must belong the the same entity group. 

\subsection{Source Image Upload}
As stated above the upload stage is the first one. The tiles to be used can be specified either via a Google search term or by allowing the application access to the user's profile on a social media site. The user's source image is stored in the Blobstore with a key and other relevant information about the job stored in a Datastore entity.

\subsection{Processing of Source Image and Tiles}
Next the processing of the source image and the tiles are performed in parallel. The stage processing the source image are done by 20 tasks in parallel each responsible for $\frac{1}{20}$th of the image. Each task divides its region of the source image into patches and calculates the average value of each of the RGB channels. When a task completes it stores these values individually as the metric of the patches in a Datastore entity referred to as a Result. The stage processing the tiles performs a similar operation but calculates the metric of the whole image since each tile will replace a patch in the final mosaic. A thumbnail of the tile is also stored together with the metric so it does not need to be fetched again when constructing the mosaic. When the tiles are specified using a Google search term 8 tasks are launched in parallel each retrieving 10 thumbnails of the Google result giving us a total of 80 tiles to build the mosaic image from.

\subsection{Mapping Tiles to Patches}
When the metrics for the source image and all of the tiles are calculated then the tiles needs to be mapped to a patch in the source image. 20 tasks each mapping a subset of patches to the closest tile performs this stage in parallel. The distance $D_{p,t}$ between a patch $p$ and a tile $t$ is defined accordingly to equation \eqref{eq:dist} where $R_i$, $G_i$ and $B_i$ are the average values of the RGB channel.

\begin{equation}
\label{eq:dist}
D_{p,t} = (R_p - R_t)^4 + (G_p - G_t)^4 + (B_p +B_t)^4
\end{equation}

The best matching tile's index is stored in the Datastore for each of the patches that a task is responsible for.

\subsection{Constructing Regions}
To speed up processing and reduce the memory impact the final mosaic is not created by a single task. Instead 20 tasks construct regions of the final mosaic by reading the Results stored by the mapping stage and the thumbnails stored by the tile processing stage. All the regions have the same size as the final mosaic but are transparent in areas where no tiles have been added in order to simplify the final composition of the mosaic.

\subsection{Constructing the Final Mosaic}
The last stage of creating the mosaic is to merge all of the regions together into the complete image. This process has to be done by a single task since it gives the single final output. The Job entity in the Datastore created when the Job was started is modified to yield a status of completed as well as holding a url to the final mosaic that has been uploaded to the Blobstore.

\subsection{Cleaning}
Finally after the mosaic is created all the intermediate Results stored in the Datastore by the processing stages, the mapping stages and the stages constructing the regions are deleted. The Regions stored in the Blobstore are also deleted since that information now resides entirely in the final mosaic.

\section{Scalability}
\label{sec:Scale}
Scalability is an essential consideration in the design of a cloud application. As described in Section \ref{sec:SysArch}, MoMoMosaic makes extensive use of Google App Engine Task Queues. The task queues are configured as push queues, which has two major consequences: (i) The tasks must be pushed to another Google App Engine service, and (ii) processing capacity is scaled automatically to meet demand. As processing is performed entirely within the Google App Engine ecosystem, the restriction of  push queue task recipients is not an issue, and the automated scaling of processing capacity is a very valuable service, removing the need for complex load testing simulations and scaling algorithms.  With processing capacity scaling dealt with by the App Engine itself, the other possible bottleneck comes in the form of I/O for memcache, the datastore and google cloud storage.

\subsection{Memcache \& Scalability}
Memcache is a ``distributed in-memory data cache''\cite{memcache} service, giving fast access to regularly used data. Read and write speed scales naturally, but capacity is limited, particularly when using free-tier services. Data stored in memcache are also liable to eviction which necessitates elegant handling of missing data. 
In MoMoMosaic, memcache is used to store counters. These counters track the progress of tasks during the parallel execution of the mosaic creation algorithm. Task counters are implemented using bitwise OR operations, so each task's completion can be uniquely identified. When updating task counters, memcache's Compare-And-Set functionality\cite{web:vanRossum-CAS} is used to detect race conditions and retry accordingly. Tasks are deemed to have failed if they have not completed after a certain time period, and any such task is relaunched. The bitwise OR implementation allows a single task to be re-executed, as opposed to the entire stage. Thanks to the idempotent design of the tasks, relaunching does not interfere with the execution flow. In the event of the loss of a memcache task counter, the datastore is queried to find the last incomplete execution stage, and the entire stage is relaunched. Stage completion is recorded in the datastore in the Job entity. We expect memcache's reliability to be good enough that a more complex re-execution strategy is unnecessary.
MoMoMosaic also uses memcache to store the results from Google Image Search API calls. The API calls are restricted to 100 per-day for free tier usage, and so this caching of results allows the service to remain semi-operational when the API call limit has been reached. As only the thumbnail URLs for the images are stored in memcache, the effect on storage capacity is minimal. In any case, the memcache's automated ageing-off of key-value pairs that have not been accessed for a certain time period provides some level of clean-up.

\subsection{Datastore \& Scalability}
The Datastore provides distributed, schemaless NoSQL data storage with support for atomic transactions and some consistency guarantees. The Datastore guarantees strong consistency of queries across an entity group, that is, all items sharing a particular ancestor. Datastore queries are implemented such that complexity scales with the number of query results, not the size of the data set being queried. Whilst this means some operations familiar from relational databases, such as JOIN operations, are not available it allows the datastore to scale to be very large without prohibitive resource costs.
MoMoMosaic makes extensive use of the Datastore to store results from the various tasks and stages involved in the process flow. For each submission to the app, a new Job entity is created, and Result entities are used by tasks to store output. Results are inserted in to the datastore with their Job as a parent. This structure allows all the Results to be queried as a single entity group, specifying the Job as an ancestor, and so guarantees strong consistency.

\subsection{Blobstore \& Scalability}
Access Control Lists
Strong Data Consistency

The Blobstore provides distributed, replicated storage for Binary Large OBjects, or blobs. The Blobstore supports files up to many terabytes in size, offering a solution to the Datastore's 1 megabyte maximum entity size. MoMoMosaic uses the Blobstore for storage of uploaded source images, mosaic regions and full mosaic images. Access to the Blobstore is slower than either Memcache or Datastore access, but given the processing times involved in creating a mosaic, Blobstore I/O operations are not a bottleneck and so do not affect scalability.


\section{Frontend Design and Implementation}
The MoMoMosaic website is designed to present a unified website throughout the mosaic generation process, and to make it as encouraging and fun as possible. Our site is aimed at users who either want to make a serious mosaic, potentially for a friend or family member, or for people with a lot of free time who want to mess around on the internet. To this end we have made it easy for even the most distracted user to generate a mosaic, as shown in figure \ref{fig:MoMoWeb} the site has a simple structure, with red "get started"  buttons on most pages.
We utilized several technologies  described in greater detail below to make this easier.

\begin{figure*}[t]
	\centering
	\includegraphics[width=\textwidth]{fig/MoMoWebDiagram2.png}
	\caption{Map of main website pages.}
	\label{fig:MoMoWeb}
\end{figure*}

\subsection{Jinja Framework}
Jinja, a web templating language, was used to generate a custom login header for every webpage, but also allowed us to use generate more complex and personalized content. For example a user gallery is generated with the latest user generated mosaics automatically, by passing the results of a Datastore query to Jinja.
The following Jinja snippet generates the user gallery on the homepage:
\begin{lstlisting}[language=HTML]
{% for job in jobs %}
<div class="4u{% if loop.index is divisibleby 3 %}${% endif %} 6u{% if loop.index is divisibleby 2 %}${% endif %}(2) 12u$(3)">
   <article class="box post">
      <a href="{{ job.mosaic_url }}=s0" class="image fit"><img src="{{ job.mosaic_url }}=s400 " alt="Image failed to load" /></a>
      <h3><a href="https://www.google.co.uk/search?q={{ job.search_term }}&tbm=isch"> {{ job.search_term }}</a></h3>
      <p>Generated on : {{ job.date.strftime('%Y-%m-%d %H:%M') }} by <i>{% if job.user_id %}{{ job.user_id }} {% else %} anonymous {% endif %}</i> from this image: </p>
      <a href="{{ job.source_url }}=s0" class="image fit"><img src="{{ job.source_url }}=s400 " alt="Image failed to load" /></a> 
   </article>
</div>
{% endfor %}
\end{lstlisting}
As can be seen, Jinja supports many features such as for loops, variables, if-else statements, and automatic formatting. This makes it ideal for quickly prototyping websites that are easy to maintain.

\subsection{Mobile Site}
Our web page is responsive, and transitions fluidly to mobile sized browsers when the browser window is resized, as shown in the screenshot comparison in figure {\bf create figure}. The fluidity is achieved by using skel.js, a grid based layout system, which automatically relocates content based on the size of the screen. These technologies allow mobile users to get the full user experience without the clunkiness of using a desktop site.{\bf ??? Since mobile users benefit massively from server side computation. ???}
Our site design was mainly provided through the "elements" html5 template (linked at the bottom of our page) so we did not have to spend a lot of time achieving a professional looking site. 

\subsection{Social Media Integration}
Many socail media sites which center on user generated content integrate other services with their own. So for example on our site the user could simply authenticate our application with either facebook, Instagram, flickr, or another photo storage website, and be instantly able to use their library of personal photos instead of uploading them all separately. These features make the user experience a lot less time consuming, since they only need to upload their photos to one photo sharing website with a developer API, and this enables them to use them on any other share-capable sites.
There are, however, problems with using social site API's. Unfortunately each website has a different API that developers must learn to use, and a lengthy list of terms and restrictions that participating sites must abide by. Facebook, for example, must approve that your application meets it's standards before it will authorise the integration of anything more than basic user information into your site. 

\section{Future considerations}
The core of the application, the algorithm that creates the mosaic, is currently very basic. A first rather simple modification that would make to mosaics more vivid is to reduce the usage of the same tile over large areas. This could be done by either penalising used tiles or just add random noise to the tile metrics when they are mapped to a patch. A more advanced modification would be to extend the metric to not only include color information but also information about edges and frequency. For this edge detection as well as a suitable transform to extract the frequency content of an image will need to be implemented alongside a new decision function taking these new parameters as input. Taking it one step further would be to do edge detection and frequency extraction of the entire source image and use variable patch sizes to concentrate the information density to where it is needed the most.

A freemium model has been discussed in order to attract users and hopefully also be able to use the more powerful, billed instances on GAE. The premium options would be available either as a monthly subscription or on a pay-as-you-go basis. Features available to paying users would be to create private mosaics, the ability to set more parameters and use larger source images as well as create mosaics with higher resolution. 

Please note that this report describes a proposed design which does not currently fully reflect the implemented one. The part using social media sites as a source for tile images is not yet implemented as well as the described parts of the webpage related to a vivid user community with comments and upvotes. These will need to be implemented for both the front end and the back end. Furthermore, the current Datastore structure is slightly simplified compared to the one described in section \ref{sec:Scale}. 

Other aspects that are not as well implemented as desired due to the time constraint is error handling and input validation. Both of these are essential in order to provide a stable system with a good user experience. Examples were it is needed is the retry schedule described in section \ref{sec:Scale} and validation of what the user inputs on the upload page. 

The current implementation of the mosaic creation is not optimized towards performance. If performance later on would be considered to have negative impact on the user experience then it might be worth to optimise the current Python implementation at first. If that is not found sufficient then switching to IaaS might be a solution. Using IaaS instead would make it possible to use a high performance language like C or C++ and further optimise the system it is run on in order to improve the performance.

\section{Conclusion}
Something smart.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\columnwidth]{fig/datastore.png}
	\caption{Schematic overview of the Datastore and the identifiers used.}
	\label{fig:datastore}
\end{figure}

\begin{figure*}[t]
\centering
\captionsetup{width=\textwidth}
\begin{minipage}{0.5\linewidth}
  \centering
  \includegraphics[width=0.8\columnwidth]{fig/desktop.png}
  \caption{Screenshot of the desktop application.}
  \label{fig:desktop}
\end{minipage}%
\begin{minipage}{0.5\linewidth}
  \centering
  \includegraphics[width=\columnwidth]{fig/mobile.png}
   \caption{Screenshots of the mobile application.}
  \label{fig:mobile}
\end{minipage}
\end{figure*}


\bibliographystyle{ieeetr}
\bibliography{ref}

\end{document}

