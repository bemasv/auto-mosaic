\documentclass[10pt, final, conference, letterpaper, twocolumn, twoside]{IEEEtran}
\usepackage[top=0.75in,bottom=1.69in,left=0.56in,right=0.56in]{geometry} % see geometry.pdf on how to lay out the page. There's lots.
\geometry{a4paper}
\usepackage{graphicx}
\usepackage{cite}
\usepackage{url}

% Date
\usepackage[yyyymmdd]{datetime}
\renewcommand{\dateseparator}{-}
\newdateformat{mydate}{\monthname[\THEMONTH] \ordinal{DAY}, \THEYEAR}

% Math
\usepackage{amsmath}
%\usepackage{mathtools}


\usepackage{listings}
\lstset{basicstyle=\small\ttfamily,breaklines=true,showstringspaces=false}

\usepackage{caption}

% Footer
%\lfoot{University of Bristol \\Cloud Computing COMSM0010}
%\cfoot{}
%\rfoot{\date}

% Title
\title{MoMoMosaic}
\author{\small Martin Andersson \\
		 Candidate No: 17725\\
		 \small University of Bristol
		 \and
	    \small Sven Hollowell \\
		 Candidate No: 17421\\
		 \small University of Bristol
		 \and
	    \small Ben Leslie \\
		 Candidate No: 17340\\
		 \small University of Bristol}
\usepackage{fancyhdr}

%%% BEGIN DOCUMENT
\begin{document}
\pagenumbering{gobble}

%	\begin{titlepage}
%		\maketitle	
%		\thispagestyle{empty} % or alph
		
%		\clearpage
%	\end{titlepage}
\pagestyle{fancy}
{\setlength{\parindent}{0.5cm}

\maketitle

\noindent
\textbf{ \emph{Abstract--} \small MoMoMosaic is a cloud computing service that provides users with custom generated photomosaics - pictures built up out of smaller pictures, or ‘tiles’. The service is hosted on Google App Engine, with a web frontend that allows user to select pictures using local files, a Google image search term, or social media sources. This report details the design and implementation of MoMoMosaic, and comments on the issues of building scalable applications in the cloud, particularly in Google's App Engine environment. The application can be run online at \url{http://momomosaic-uob.appspot.com/index.html}}
\\

\noindent
\textbf{ \emph{Keywords--} Cloud computing, photomosaic, Google App Engine, embarrassingly parallel, image processing, social web.}

\section{Introduction}
MoMoMosaic brings new image processing techniques to user content driven social media, by allowing users to create image mosaics. In an image mosaic, a user uploads a source image to be converted to a mosaic, and a library of tile images is used to try and rebuild the source image. An example is shown in Fig. \ref{fig:example}. 

\begin{figure}[!ht]
	\centering
	\includegraphics[height=0.5\columnwidth]{fig/mosaiccatdiagram.png}
	\caption{An Example of an Image Mosaic}
	\label{fig:example}
\end{figure}

There exist several online image mosaic creation services\cite{web:Mosaically}\cite{web:EasyMoza}\cite{web:bighugelabs}, but all require explicit specification of tile image libraries - MoMoMosaic simplifies image mosaic creation by giving users the ability to use verbal descriptions of tile images. These verbal descriptions are fed to Google's image search algorithm to source a variety of tile images, thus bypassing the need to upload or specify a large custom library of tiles. Alternatively, users can select images from their own Facebook, Flickr or Instagram accounts. With the inputs established, the image processing itself is performed server-side. 

Thanks to the embarassingly parallel nature of many of the steps in our image mosaic algorithm, MoMoMosaic employs a scale-out model to cope with increased demand, thus leveraging the advantages of cloud computing infrastructure. Provided a user has logged-in to the website, the resulting image mosaic is added to a user gallery and is retrievable on future visits. Mosaics from a user's gallery can be submitted to a public gallery, where other users of the service can rate the mosaic, with the top rated public mosaics being displayed on the front page. 

MoMoMosaic helps foster a community by allowing users to rate and comment on other user's public mosaics, and giving users the ability to `follow' one another, and so be notified of their future public submissions.


\section{Infrastructure vs. Platform as a Service}
There are currently two leading cloud computing providers, Amazon and Google, with major companies such as Microsoft and IBM forming a part of the competition. Historically choosing between the two has been synonymous with choosing Infrastructure as a Service (IaaS) or Platform as a Structure (PaaS). Amazon, with their Amazon Web Services (AWS) \cite{AWS}, have been providing IaaS and Google PaaS through the Google App Engine (GAE) \cite{GAE}. Recently, Google has also started to offer IaaS through their Google Compute Engine \cite{compute_engine} and likewise Amazon have begun offering AppStream \cite{appstream}, which resembles PaaS.

IaaS essentially provides access to virtual machines that can be accessed remotely. In doing so, it gives the developer full control over, and responsibility for, software such as databases, programming languages and so forth. It also requires the developer to configure the system properly for their purposes, and so, in the case of a complex infrastructure, requires a great deal of administrative work.

PaaS abstracts infrastructure and administrative choices away from the developer, providing an environment in which certain services are preconfigured for the developer. A PaaS provider typically makes choice of OS, database, web-server etc., instead providing to the developer a set of tools for interfacing with these services. PaaS has the advantage that configuration is largely automated for the developer, and as such products can be shipped much more quickly. The low administrative workload comes at the cost of flexibility.

When mocking-up a design for the implementation of MoMoMosaic, it was conlcuded that all of the necessary tools were available through Google's App Engine. Given the reduced administrative workload and development time associated with PaaS, GAE was chosen to host MoMoMosaic. Furthermore, the Google Seach API has an essential role in the application, so  latency benefits may be drawn from running the application in Google's data centres. Only GAE and AWS were considered as potential candidates because of their strong market position, which gives rise to an extensive user base and well documented features, as well as a large support community.

\section{System Architecture}
\label{sec:SysArch}
Applications on GAE are run on stateless virtual machines referred to as instances. These instances are sandboxed but can via the GAE environment access a wide range of storage, services and tools. Four different frontend instances with varying performance and consequently varying pricing are available.

The core of the application is the MoMoMosaic algorithm, which utilises several of the different storage alternatives, services and tools available through GAE as well as APIs supplied by social media websites. An overview of the system architecture is shown in figure \ref{fig:sys_arch} and the components depicted therein are discussed in further detail below. MoMoMosaic is built around URL handlers, written in Python, which control the process flow between the various services involved.

\begin{figure}[!ht]
	\centering
	\includegraphics[height=0.6\columnwidth]{fig/system_architecture.png}
	\caption{Overview of the system architecture for MoMoMosaic.}
	\label{fig:sys_arch}
\end{figure}

\subsection{Storage}
Three levels of storage with different properties and restrictions are in use. Memcache provides a distributed data cache with high read and write speeds. Data stored in Memcache is not guaranteed to be persistent and the maximum size of an entry is slightly below 1 MB \cite{memcache}. Memcache is the closest approximation to RAM on the Google App Engine.

The NDB Datastore, from herein referred to simply as the Datastore, is a schemaless object datastore that acts as persistent storage for GAE applications. It supports automatic caching of query results, complex queries and nested data structures. It should be stressed, however, that the Datastore is not a relational database. Built upon Google's BigTable technology, the Datastore is implemented like a distributed, sorted hash table with keys pointing to \emph{entities}. This implementation provides increased scalability, but does restrict the possible queries. This is discussed further in section \ref{ssec:DatastoreScale}. The Datastore is also limited to entities with a maximum size of 1 MB, with total storage of 1 GB available to free-tier services \cite{datastore}.

The Datastore allows a data hierarchy to be formed by specifying parent keys for new entities. All entities sharing a common ancestor then belong to a single entity group, within which the Datastore guarantees strong consistency. To maintain this strong consistency, entity groups are stored on a single Datastore node and write operations lock the entire entity group. Writes to an entity group are therefore much more strictly rate limited, at an average of 1 operation per second. MoMoMosaic's predictable process flow has no need for ancestor queries and so entity groups are avoided.   

Data larger than 1 MB is stored in the Blobstore. The Blobstore's read and write access is slower than either the Datastore or Memcache. Operations on Blobstore items are limited to write, read or delete, and all items must be accessed by a key. The Blobstore has a convenient feature where it can serve a URL giving direct access to a stored image \cite{blobstore}. Blob data are created indirectly through the use of HTTP POST requests, so in order for the application to write large data files the Google Cloud Storage Client Library was used which provides an interface enabling write operations to the Blobstore from within the application \cite{gcs}. Access to the Blobstore is controlled using Access Control Lists. This allows the implementation of public and private data for users of MoMoMosaic.

\subsection{Google App Engine Services}
The GAE environment gives access to several different services of which Task Queues are the most heavily used by the MoMoMosaic application. MoMoMosaic's use of Task Queues allows us to benefit from some of the advantages of cloud computing, as well as dealing with response time issues. Normal HTTP requests have a response deadline of 60 seconds in GAE and can not be performed in the background. By adding tasks to one or several different queues, the Task Queue API allows for background processing initiated by user requests with a response deadline of 10 minutes. Two different types of queues can be used: push queues which are processed within the GAE based on a pre-set rate and pull queues where task consumers, residing inside or outside the GAE, can lease tasks for processing during a specific time frame. The push queue met all of our needs without the added complexity of the pull queue so the former is what has been used for implementation. \cite{taskqueue}

Three other services from the GAE envirnoment are also worth mentioning. Images is used to handle images internally in the application, including analysis and composition of images for the mosaic. To access external URLs and retrieve the results from requests to these sites the URL Fetch service was used. The Users service gave an easy way of identifying users and handling authentication by through Google's account services.

\subsection{External Libraries and APIs}
APIs from social media sites allow us to interact and use images already uploaded by the user to other services. The sites proposed for integration are Facebook, flickr and Instagram, although any site with an API that allows image retrieval could potentially be supported by the application. To retrieve images based on a Google query the Google Search API's complex query is utilised.

MoMoMosaic is implemented in Python 2.7, as supported by the GAE environment. There are a limited number of external modules available for use on the GAE, three of which are particularly crucial to the operation of the application. \texttt{PIL} is used for processing images, \texttt{webapp2} provides a web framework and \texttt{jinja2} is used for HTML rendering. 

\section{Design \& Process Flow}
\label{sec:Design}
In this section, the process of mosaic creation is detailed from the point at which a user submits a request to the delivery of the final mosaic.

A Job is created when a user has uploaded a source image and specified a tile library. An overview of this naming convention of the different components is given in figure \ref{fig:components}.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.9\columnwidth]{fig/components.png}
	\caption{Overview depicting our naming convention of different components.}
	\label{fig:components}
\end{figure}

The creation of a mosaic, the Job, is divided into several stages each executed by either a single task or several running in parallel. The complete process flow is depicted in figure \ref{fig:process_flow}.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.9\columnwidth]{fig/process_flow.png}
	\caption{Process flow for creation of a mosaic.}
	\label{fig:process_flow}
\end{figure}

Before each stage can be launched the previous one has to complete. This is handled by using a separate counter for each of the stages of an individual job, with each counter capable of tracking the completion of the tasks belonging to that stage. Furthermore wait handlers are implemented that checks the status of the relevant counter and launch the next stage at the correct time. A separate Task Queue for each of the stages described below as well as one for the wait handlers enables us to initiate the whole execution at once and then let the wait handlers add tasks to the relevant queue when they are ready to be executed.

Note that the intermediate Results Job stored in the Datastore by the different stages all belong to the same entity group with. But Results form different Jobs belong to different entity group since they have their Job as ancestor. Because the need of being able to make a query across all Jobs they must belong the the same entity group.

\subsection{Source Image Upload}
As stated above, the upload stage is the first one. The tiles to be used can be specified either via a Google search term or by allowing the application access to the user's profile on a social media site. The user's source image is stored in the Blobstore, with the Blobstore key and other relevant information about the job stored in a Datastore entity.

\subsection{Processing of Source Image and Tiles}
Next the processing of the source image and the tiles are initialized simultaneously. Processing the uploaded source image is done by 20 tasks in parallel. Each task divides its region of the source image into patches and calculates the average value for red, green, and blue channels separately. When a task completes it stores the results of its processing in a Datastore entity referred to as a Result.

To process a Google search term 8 tasks are launched in parallel each retrieving 10 thumbnails from Google's image search, giving us a total of 80 tiles with which to build the mosaic image. Processing of the tiles is done similarly to the patches, but additionally stores a thumbnail of the tile along with the RGB metrics. 

\subsection{Mapping Tiles to Patches}
When the metrics for the source image patches and all of the tiles are calculated, a suitable tile needs to be selected for each patch. We choose the best tile by minimizing the distance $D_{p,t}$ of the $R$, $G$ and $B$ values of tile $t$ and patch $p$, taken to the 4th power, according to equation \eqref{eq:dist}. This simple algorithm can be performed in parallel and gives reasonable results. The 4th power was found to give the most aesthetically pleasing results by visual comparison. We playfully refer to our algorithm on our website as the \emph{crap-reduce} algorithm, for potential use in future marketing campaigns.

\begin{equation}
\label{eq:dist}
D_{p,t} = (R_p - R_t)^4 + (G_p - G_t)^4 + (B_p +B_t)^4
\end{equation}

We launch 20 tasks, each finding a closest tile match for some subset of the patches, to perform this stage in parallel. Once a task has completed this matching process for all its allocated patches, the mappings are recorded in a Result in the Datastore.

\subsection{Constructing Regions}
We found during testing that the final image construction was a bottleneck in our program. To speed up processing and reduce the memory footprint, we use 20 tasks to construct each region of the final mosaic. This is achieved by reading the Results calculated by the mapping stage and fetching the appropriate thumbnails stored by the tile processing stage. All the regions have the same total dimensions as the final mosaic, but are transparent in areas outside the tasks allocated region.

\subsection{Constructing the Final Mosaic}
The last stage of creating the mosaic is to merge all of the regions together into the completed photomosaic. This process has to be done by a single task, since it requires write access to the final image file. After this stage is completed, the relevant Job entity in the Datastore is modified to yield a status of completed, as well as holding an image serving url for the Blobstore location at which the final mosaic is stored.

\subsection{Cleaning}
Now that the mosaic construction is completed, we remove all the intermediate Results stored in the Datastore by the processing stage, the mapping stage and the region construction stage. This prevents unnecessary data being stored in the Datastore, which would otherwise use up our storage quota.

\section{Scalability}
\label{sec:Scale}
Scalability is an essential consideration in the design of a cloud application. As described in Sections \ref{sec:SysArch}, \ref{sec:Design}, MoMoMosaic makes extensive use of Google App Engine Task Queues. The task queues are configured as push queues, which has two major consequences: (i) The tasks must be pushed to another Google App Engine service, and (ii) processing capacity is scaled automatically to meet demand. As our processing is performed entirely within the Google App Engine ecosystem, the restriction of  push queue task recipients is not an issue, and the automated scaling of processing capacity is a very valuable service, reducing the need for complex load testing simulations and scaling algorithms.  

With processing capacity scaling dealt with by the App Engine itself, the other possible bottleneck comes in the form of I/O for Memcache, the Datastore and the Blobstore. 

\subsection{Memcache \& Scalability}
Memcache is a ``distributed in-memory data cache''\cite{memcache} service, giving fast access to regularly used data. Read and write speeds scale by design, but overall capacity is limited, particularly when using free-tier services. Data stored in Memcache are also liable to eviction without warning, and so it is necessary to design elegant handling of missing Memcache data. 

In MoMoMosaic, Memcache is primarily used to store counters. These counters track the progress of tasks during the parallel execution of the mosaic creation algorithm. Task counters are implemented using bitwise OR operations, so each task's completion can be uniquely identified. When updating task counters, Memcache's Compare-And-Set functionality\cite{web:vanRossum-CAS} is used to detect race conditions and retry accordingly.

Tasks are deemed to have failed if they have not completed after a certain time period, and any such task is relaunched. The bitwise OR implementation of the task counters allows a single task to be re-executed, as opposed to the entire stage. Thanks to the idempotent design of the tasks, relaunching does not interfere with the execution flow. In the event of the loss of a Memcache task counter, the Datastore is queried to find the last incomplete execution stage, and the entire stage is relaunched. Stage completion is recorded in the Datastore in the Job entity. We expect Memcache's reliability to be good enough that a more complex re-execution strategy is unnecessary.

MoMoMosaic also uses Memcache to store the results from Google Image Search API calls. The API calls are restricted to 100 per-day for free tier usage, and so this caching of results allows the service to remain semi-operational when the API call limit has been reached. As only the thumbnail URLs for the images are stored in Memcache, the effect on storage capacity is minimal. In any case, the Memcache's automated ageing-off of key-value pairs that have not been accessed for a certain time period provides some level of clean-up.

\subsection{Datastore \& Scalability}
\label{ssec:DatastoreScale}
The Datastore provides highly scalable persistent storage. Thanks to the hash-table like design, Datastore queries are implemented such that complexity scales with the number of query results, not the size of the data set being queried. Provided entity groups are not allowed to become too large, an issue dealt with in MoMoMosaic's design, the Datastore should scale to well beyond realistic usage figures.

MoMoMosaic makes extensive use of the Datastore to store results from the various tasks and stages involved in the process flow. For each submission to the application, a new Job entity is created which keeps track of which stages of the process flow have been completed and various pieces of book-keeping information. Tasks themselves create Result entities to store their output. As all parameters of the Job are determined prior to execution, all of the entities associated with a Job can be given related identifying keys. This allows them to be stored as root entities in the Datastore, minimising the use of entity groups and making best use of the low complexity lookup operations in the Datastore. 

The key based association between root entities and the minimisation of entity group sizes in the Datastore are illustrated in figure \ref{fig:datastore}.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.94\columnwidth]{fig/datastore.png}
	\caption{Schematic overview of the Datastore and the identifiers used.}
	\label{fig:datastore}
\end{figure}

\subsection{Blobstore \& Scalability}
Access Control Lists
Strong Data Consistency

The Blobstore provides distributed, replicated storage for Binary Large OBjects, or blobs. The Blobstore supports files up to many terabytes in size, offering a solution to the Datastore's 1 megabyte maximum entity size. MoMoMosaic uses the Blobstore for storage of uploaded source images, mosaic regions and full mosaic images. Free-tier GAE applications are given 5GB of storage space on the Blobstore. Access to the Blobstore is slower than either Memcache or Datastore access, but given the processing times involved in creating a mosaic, Blobstore I/O operations are not a bottleneck and so do not affect scalability. A single Blobstore request can serve a maximum of 32 megabytes of data and MoMoMosaic's current implementation doesn't make any allowances for files greater than this size, although we don't foresee this being problematic.

\section{Frontend Design and Implementation}
\begin{figure*}[t]
	\centering
	\includegraphics[width=\textwidth]{fig/MoMoWebDiagram2.png}
	\caption{Map of main website pages.}
	\label{fig:MoMoWeb}
\end{figure*}

The MoMoMosaic website was designed to present a unified website throughout the mosaic generation process, and to make it as encouraging and fun as possible. Our site is aimed at users who either want to make a serious mosaic, potentially for a friend or family member, or for people with a lot of free time who want to mess around on the internet. To this end we have attempted to make it as intuitive as possible. The site has a simple structure, with red "get started"  buttons in both the user gallery and the homepage, and another red "let's go" button on the upload page, to ensure even the most distractable user will know where to click in order to progress.

\subsection{User Walkthrough}
The site progresses linearly as shown in figure \ref{fig:MoMoWeb}. On first visiting the site the user should land on the homepage, and, being presented with numerous reasons and examples of mosaics as they scroll down the site, will hopefully choose to either start making a mosaic or login using their Google account. The user hits "get started" and is then taken to the upload page where they must choose a file to upload, choose a custom search term, and then click "let's go". The user may also choose to login at this point, and possibly read the instructions if they get confused. 

Once the image has uploaded we redirect them to a waiting page where their result will be loaded. The results page uses a unique job id to keep track of separate jobs, so the page can be closed and reopened, or even bookmarked for later viewing. The mosaic generation can take approximately 3 minutes for a decently sized image, potentially leaving the user bored, so we chose to place our "learn more" information on this page to keep them entertained (or possibly even more bored depending on their level of interest in cloud computing technologies).

The result page sends a request to the server every 5 seconds to check if the mosaic has been completed. The request result is then sent back as text and parsed using JavaScript. If the mosaic is complete the request will also return a URL to the generated image, and this will update the page so the image is displayed, as well  as updating the text on the page to say "completed". The requests are handled in the background making the process unobtrusive to the user, however the process could be improved further by adding a loading bar.

The completed mosaic is displayed on the result page, the users personal gallery (if logged in), and, until newer mosaics displace it, in the user gallery section on the homepage. This allows users to get inspiration from others, and store all their mosaics conveniently. 

Although users can see the most recent mosaics, the site is still not truly "social" until social features are implemented. We originally planned to have an upvote/downvote and commenting functionality similar to reddit.com, these would allow visitors to easily find the best mosaics, and allow for feedback and a sense of user community. 

We utilized several technologies to make our frontend development easier:
\subsection{Jinja Framework}
Jinja, a web templating language, was used to generate a custom login header for every webpage, but also allowed us to use generate more complex and personalized content. For example a user gallery is generated with the latest user generated mosaics automatically, by passing the results of an NDB query to Jinja.
The following Jinja snippet generates the user gallery on the homepage:
\begin{lstlisting}[language=HTML]
{% for job in jobs %}
<div class="4u{% if loop.index is divisibleby 3 %}${% endif %} 6u{% if loop.index is divisibleby 2 %}${% endif %}(2) 12u$(3)">
   <article class="box post">
      <a href="{{ job.mosaic_url }}=s0" class="image fit"><img src="{{ job.mosaic_url }}=s400 " alt="Image failed to load" /></a>
      <h3><a href="https://www.google.co.uk/search?q={{ job.search_term }}&tbm=isch"> {{ job.search_term }}</a></h3>
      <p>Generated on : {{ job.date.strftime('%Y-%m-%d %H:%M') }} by <i>{% if job.user_id %}{{ job.user_id }} {% else %} anonymous {% endif %}</i> from this image: </p>
      <a href="{{ job.source_url }}=s0" class="image fit"><img src="{{ job.source_url }}=s400 " alt="Image failed to load" /></a> 
   </article>
</div>
{% endfor %}
\end{lstlisting}
As can be seen, Jinja supports many features such as for loops, variables, if-else statements, and automatic formatting. This makes it ideal for quickly prototyping websites that are easy to maintain.

\subsection{Mobile Site}
Our web page is responsive, and transitions fluidly to mobile sized browsers when the browser window is resized, as shown in the screenshot comparison given in figures \ref{fig:desktop} and \ref{fig:mobile}. The fluidity is achieved by using skel.js, a grid based layout system, which automatically relocates content based on the size of the screen. These technologies allow mobile users to get the full user experience without the clunkiness of using a desktop site. 

Since mobile users stand to gain most from server side computation we felt it was important to make sure that it could be used on devices with smaller screens. It is possible for a mobile user to take a photograph from their built-in camera, upload it to our application and in a few minutes have access to a mosaic consisting of thousands of images. Performing computation on the server will also benefit mobile users by reducing battery drain.

\begin{figure*}[t]
\centering
\captionsetup{width=\textwidth}
\begin{minipage}{0.5\linewidth}
  \centering
  \includegraphics[width=0.8\columnwidth]{fig/desktop.png}
  \caption{Screenshot of the desktop application.}
  \label{fig:desktop}
\end{minipage}%
\begin{minipage}{0.5\linewidth}
  \centering
  \includegraphics[width=\columnwidth]{fig/mobile.png}
   \caption{Screenshots of the mobile application.}
  \label{fig:mobile}
\end{minipage}
\end{figure*}

\subsection{Template}
Instead of writing a HTML webpage from scratch, which would be inordinately time consuming, we chose to build on top of an existing online template. We chose to modify the "Elements" HTML5 template provided by \cite{template}, these templates are free to use provided you leave a link to their site at the bottom of your webpage. The template is a quick way of creating a professional looking, responsive, and easily extensible website in a limited amount of time. 

\subsection{Social Media Integration}
Many social media sites which focus on user generated content integrate other services with their own. So for example on our site the user would be able to authenticate our application with either Facebook, Instagram, flickr, or another photo storage website, and be easily able to use their library of personal photos instead of uploading them all separately. These features would make the user experience a lot less time consuming, since they only need to upload their photos to one photo sharing website with a developer API.


There are, however, problems with using API's from other websites. Unfortunately each website has a different API that one must learn to use, and a lengthy list of terms and conditions that participating sites must abide by. Facebook, for example, must approve that your application meets its standards before it will authorise the integration of anything more than basic user information into your site. \cite{fb}

\section{Future considerations}
The core of the application, the algorithm that creates the mosaic, is currently very basic. A first rather simple modification that would make to mosaics more vivid is to reduce the usage of the same tile over large areas. This could be done by either penalising used tiles or just add random noise to the tile metrics when they are mapped to a patch. A more advanced modification would be to extend the metric to not only include color information but also information about edges and frequency. For this, edge detection as well as a suitable transform to extract the frequency content of an image will need to be implemented alongside a new decision function taking these new parameters as input. Taking it one step further would be to do edge detection and frequency extraction of the entire source image and use variable patch sizes to concentrate the information density to where it is needed the most.

A freemium model has been discussed in order to attract users and hopefully also be able to use the more powerful, billed instances on GAE. The premium options would be available either as a monthly subscription or on a pay-as-you-go basis. Features available to paying users would be to create private mosaics, the ability to set more parameters and use larger source images as well as create mosaics with higher resolution. Some advertising revenue could be created, with a natural affiliation seeming to arise with photo printing services.

Please note that this report describes a proposed design which does not currently fully reflect the implemented one. Those aspects using social media sites as a source for tile images are not yet implemented as well as the described parts of the webpage related to a vivid user community with comments and upvotes. These will need to be implemented for both the front end and the back end. Furthermore, the current Datastore structure is slightly simplified compared to the one described in section \ref{ssec:DatastoreScale}.

Other aspects that are not as well implemented as desired due to the time constraint is error handling and input validation. Both of these are essential in order to provide a stable system with a good user experience. Examples where error handling is needed is the retry schedule described in section \ref{sec:Scale} and validation of user input is needed on the upload page. 

The current implementation of the mosaic creation is not optimized towards performance. If, in the future, performance is deemed to have a negative impact on the user experience then there are some optimisations to the current Python implementation which could be made. An alternative would be to utilise pull queues and move processing off of the App Engine to an IaaS provider. An IaaS-run backend would make it possible to use a compiled language, such as C++, and to optimise the system configuration.


\section{Conclusion}
Google's App Engine provides a convenient and powerful environment in which to develop scale-out applications. With services built around Google's proprietary technologies, e.g. Datastore's BigTable underpinnings, a small team of developers can leverage the scalability expertise of one of the world's largest companies. This provides clear benefits for start-up companies looking to undergo rapid expansion, particularly when combined with the lack of infrastructure investment.

Google App Engine is not without its shortcomings. Within the Python environment, the limited availability of modules was somewhat restrictive for image processing. For more advanced image processing, the ability to use established libraries such as OpenCV would be very beneficial. In terms of the development process, the SDK's single threaded operation meant the parallel aspects of cloud operation could not be emulated, requiring on-line testing of the application.

To best benefit from the Google App Engine, it is necessary to design with scaling in mind from the ground up. Although parallel programming is no longer foreign to many programmers, the distributed programming necessary for cloud computing is still a relatively new paradigm. 

Given that our small team of three developers were able to design, implement and deploy a cloud application utilising the power and scalability of Google's App Engine proves that the possibility of serving a large user base is no longer limited to large companies with the capital for IT infrastructure investment.

\bibliographystyle{ieeetr}
\bibliography{ref}

\end{document}


