import os
import urllib

from google.appengine.api import memcache
from google.appengine.api import users
from google.appengine.ext import ndb
from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.api import images as GAEimages

from PIL import Image

import jinja2
import webapp2
import json as simplejson
import urllib2
import pickle

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

DEFAULT_GUESTBOOK_NAME = 'default_guestbook'
SEARCH_ENGINE_ID = '007322017615361698569:8phumj42uhk' #Sven's key
#SEARCH_ENGINE_ID = '008766034830262081045%3Aicjof9bog9g' #martin
API_KEY = 'AIzaSyB58wvcuSS5DtSuse897MFNY1h_0n2cGvk'  # Sven's key
#API_KEY = 'AIzaSyBFxg2o7hPQDPU6Bt8sBp6FcstUhQiQW3Y'  # Martin's key

SEARCH_ENGINE_ID = '008826149060983877342:tjbwswopjpa'
API_KEY = 'AIzaSyAhiZeKY5NfjVN1pTZBkj-UxLW0t7IVmKQ' # Ben's Key


# We set a parent key on the 'Greetings' to ensure that they are all in the same
# entity group. Queries across the single entity group will be consistent.
# However, the write rate should be limited to ~1/second.

def guestbook_key(guestbook_name=DEFAULT_GUESTBOOK_NAME):
    """Constructs a Datastore key for a Guestbook entity with guestbook_name."""
    return ndb.Key('Guestbook', guestbook_name)

class SearchTermResults(ndb.Model):
    """ Models the results of a call to the image search API, storing the URL
    of the thumbnail images that are returned
    """
    pickled_thumbnail_url_list = ndb.PickleProperty
    date = ndb.DateTimeProperty(auto_now_add=True)

def get_thumbnail_urls(search_terms):
    """
    get_thumbnail_urls(search_terms)

    Implements caching of results from custom search API calls to reduce number of calls.

    :param search_terms: Input query to the google image search API
    :return: thumbnail_url_list: List of urls from performing an image search on search_terms
    """
    search_terms_id = ("%s:image_search" % search_terms)
    pickled_thumbnail_url_list = memcache.get(search_terms_id)
    if pickled_thumbnail_url_list is not None:
        thumbnail_url_list = pickle.loads(pickled_thumbnail_url_list)
        return thumbnail_url_list
    else:
        search_results = SearchTermResults.get_by_id(search_terms_id)
        if search_results is not None:
            pickled_thumbnail_url_list = search_results.pickled_thumbnail_url_list
            memcache.add(search_terms_id, search_results.pickled_thumbnail_url_list)
            thumbnail_url_list = pickle.loads(pickled_thumbnail_url_list)
            return thumbnail_url_list
        else:
            thumbnail_url_list = []
            url_search_term = urllib.quote(search_terms)
            url = 'https://www.googleapis.com/customsearch/v1?q=' + url_search_term + '&cx=' + SEARCH_ENGINE_ID + '&fileType=jpg&searchType=image&key=' + API_KEY
            print url
            request = urllib2.Request(url, None, {'Referer': 'testing'})
            response = urllib2.urlopen(request)
            # Get results using JSON
            results = simplejson.load(response)
            data = results['items']
            for item in data:
                thumbnail_url_list.append(item['image']['thumbnailLink'])
            pickled_results = pickle.dumps(thumbnail_url_list)
            memcache.add(search_terms_id, pickled_results)
            image_search_results_item = SearchTermResults(id=search_terms_id)
            image_search_results_item.pickled_thumbnail_url_list = pickled_results
            image_search_results_item.put()
            return thumbnail_url_list


class Greeting(ndb.Model):
    """Models an individual Guestbook entry with author, content, and date."""
    author = ndb.UserProperty()
    content = ndb.StringProperty(indexed=False)
    blobKey = ndb.BlobKeyProperty(required=False)
    servingUrl = ndb.StringProperty()
    date = ndb.DateTimeProperty(auto_now_add=True)


class MainPage(webapp2.RequestHandler):

    def get(self):
        guestbook_name = self.request.get('guestbook_name',
                                          DEFAULT_GUESTBOOK_NAME)
        query_params = {'guestbook_name': guestbook_name}
        upload_url = blobstore.create_upload_url('/sign?' + urllib.urlencode(query_params))
        greetings_query = Greeting.query(
            ancestor=guestbook_key(guestbook_name)).order(-Greeting.date)
        greetings = greetings_query.fetch(10)

        if users.get_current_user():
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template_values = {
            'greetings': greetings,
            'guestbook_name': urllib.quote_plus(guestbook_name),
            'url': url,
            'url_linktext': url_linktext,
            'upload_url': upload_url,
        }

        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(template_values))


class Guestbook(blobstore_handlers.BlobstoreUploadHandler):

    def post(self):
        # We set the same parent key on the 'Greeting' to ensure each Greeting
        # is in the same entity group. Queries across the single entity group
        # will be consistent. However, the write rate to a single entity group
        # should be limited to ~1/second.
        guestbook_name = self.request.get('guestbook_name',
                                          DEFAULT_GUESTBOOK_NAME)
        greeting = Greeting(parent=guestbook_key(guestbook_name))

        if users.get_current_user():
            greeting.author = users.get_current_user()

        greeting.content = self.request.get('content')
        upload_files = self.get_uploads('file')  # 'file' is file upload field in the form
        if upload_files:
            blob_info = upload_files[0]
            greeting.blobKey = blob_info.key()
            greeting.servingUrl = GAEimages.get_serving_url(blob_info.key(), size=64, crop=False, secure_url=None) #crop=false -> resize=True
        else :
            urls = get_thumbnail_urls(greeting.content)
            greeting.servingUrl = urls[0]  # change 0 to 1,2, or 3 to get the second, third, or fourth search result

        greeting.put()

        query_params = {'guestbook_name': guestbook_name}
        self.redirect('/') #?' + urllib.urlencode(query_params))


application = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/sign', Guestbook),
], debug=True)
