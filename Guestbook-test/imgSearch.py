import time
import urllib2
import webapp2
import json as simplejson

SEARCH_ENGINE_ID = '008766034830262081045%3Aicjof9bog9g' # Martin
API_KEY = 'AIzaSyBFxg2o7hPQDPU6Bt8sBp6FcstUhQiQW3Y'  # Martin's key
#APIKey = 'AIzaSyB58wvcuSS5DtSuse897MFNY1h_0n2cGvk' # Sven

class MainPage(webapp2.RequestHandler) :
    def get(self, searchquery):
        # Define search term
        search_term = "BeMaSv"
        #
        if searchquery :
            search_term = searchquery

        search_term = urllib2.quote(search_term)

        # Set count to 0
        count = 0
        self.response.out.write('<html><body>')

        for i in range(0,2):
            # Notice that the start changes for each iteration in order to request a new set of images for each loop
            url = 'https://www.googleapis.com/customsearch/v1?q=' + search_term + '&cx=' + SEARCH_ENGINE_ID + '&fileType=jpg&searchType=image&start=' + str(1+i*10) + '&key=' + API_KEY
            print url
            request = urllib2.Request(url, None, {'Referer': 'testing'})
            response = urllib2.urlopen(request)

            # Get results using JSON
            results = simplejson.load(response)
            items_data = results['items']  #parse through all the JSON,

            # Iterate for each result and get unescaped url
            for item_data in items_data:
                count = count + 1
                tbLink = item_data['image']['thumbnailLink']
                self.response.out.write('<img src="%s">' % tbLink)

            # Sleep for one second to prevent IP blocking from Google
            time.sleep(1)

        self.response.out.write('</body></html>')

application = webapp2.WSGIApplication([
    ('/search/([^/]+)', MainPage),
], debug=True)